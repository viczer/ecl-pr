package example;

public class Etudiant extends Personne{
	private String niveau;
	
	

	public Etudiant(String nom, String prenom, String niveau) {
		super(nom, prenom);
		this.niveau = niveau;
	}

	public String getNiveau() {
		return niveau;
	}

	public void setNiveau(String niveau) {
		this.niveau = niveau;
	}

	@Override
	public String toString() {
		return "Etudiant [niveau=" + niveau + ", getNiveau()=" + getNiveau() + ", getNom()=" + getNom()
				+ ", getPrenom()=" + getPrenom() + ", toString()=" + super.toString() + ", getClass()=" + getClass()
				+ ", hashCode()=" + hashCode() + "]";
	}

	
	

}
