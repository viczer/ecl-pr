package example;

public class Humain<T>{
	private T var;

	

	public Humain(T var) {
		super();
		this.var = var;
	}

	public Humain() {
		// TODO Auto-generated constructor stub
	}

	public T getVar() {
		return var;
	}

	public void setVar(T var) {
		this.var = var;
	}

	@Override
	public String toString() {
		return "Humain [var=" + var + "]";
	}
	
	

}
