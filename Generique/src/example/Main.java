package example;

import example.Humain;

import java.util.ArrayList;
import java.util.List;

import example.Etudiant;
import example.Personne;
import example.Voiture;
public class Main {

	public static void main(String[] args) {
		Humain<Personne> humain = new Humain();
		Humain<Etudiant> humain2 = new Humain();
		Humain<Voiture> humain3 = new Humain();
		
		humain.setVar(new Personne("Viktoria","Vikki"));
		System.out.println(humain);
		
		humain2.setVar(new Etudiant("Fabrice", "fabbry", "bac+5"));
		humain3.setVar(new Voiture("Golf"));
		
		List<Humain> hum = new ArrayList<Humain>();
		hum.add(humain);
		hum.add(humain2);
		hum.add(humain3);
		
		for(int i = 0; i<3; i++) {
			System.out.println(hum.get(i));
		}
		
		hum.forEach(System.out::println);
//		Operation <Integer> operation1 = new Operation<Integer>(5,3);
//		operation1.plus();
//		Operation <String> operation2 = new Operation<String>("bon","jour");
//		operation2.plus();
//		Operation <Double> operation3 = new Operation<Double>(5.2,3.8);
//		operation3.plus();
//		Operation <Boolean> operation4 = new Operation<Boolean>(true, false);
//		operation4.plus();
//					
		
		
//		Example<Integer, Integer> entier = new Example<Integer, Integer>();
//		entier.setVar1(10);
//		entier.setVar2(12);
//		System.out.println(entier.getVar1().getClass().getTypeName() + " " + entier.getVar1());
//		System.out.println(entier.getVar2().getClass().getTypeName() + " " + entier.getVar2());
//		
//		
//		Example<String, String> chaine = new Example<String, String>();
//		chaine.setVar1("Bonjour");
//		chaine.setVar2("Bonjour");
//		
//		System.out.println(chaine.getVar1().getBytes().getClass().getTypeName() + " " + chaine.getVar1());
//		System.out.println(chaine.getVar2().getBytes().getClass().getTypeName() + " " + chaine.getVar2());
//		
//		Example<Double, String> couple = new Example<Double, String>();
//		couple.setVar1(10.1);
//		couple.setVar2("Welcome");
//		
//		System.out.println(couple.getVar1().getClass().getTypeName() + " " + couple.getVar1());
//		System.out.println(couple.getVar2().getClass().getTypeName() + " " + couple.getVar2());
//		
		
		
	}

}
