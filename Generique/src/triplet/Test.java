package triplet;

public class Test {

	public static void main(String[] args) {
		Triple<Integer> t1 = new Triple<Integer>(27, 28, 29);
		t1.affiche();
		Triple<String> t2 = new Triple<String>("Asma", "Damian", "Fabrice");
		t2.affiche();
		Triple<Float> t3 = new Triple<Float>(27.7F, -27.7F, 15.5F);
		t3.affiche();
	}


}
