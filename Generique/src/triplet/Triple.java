package triplet;

public class Triple<T> {
	private T premier;
	private T second;
	private T troisieme;
 
	public Triple(T premier, T second, T troisieme) {
		this.premier = premier;
		this.second = second;
		this.troisieme = troisieme;
	}
 
	public void affiche() {
		System.out.println(this.premier + " " + this.second + " "
				+ this.troisieme);
	}
}

