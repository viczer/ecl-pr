package org.eclipse.main;

import org.eclipse.model.Enseignant;
import org.eclipse.model.Etudiant;
import org.eclipse.model.Personne;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

public class App {
	public static void main( String[] args )
    {
    	/* Personne */
		System.out.println("Step 1");
    	Personne personne = new Personne();
    	personne.setNom("Guardiola");
    	personne.setPrenom("Pep");
    	System.out.println("Step 2" + personne);
    	/* Enseignant */
    	Enseignant enseignant = new Enseignant(null, null, 0);
    	enseignant.setNom("Ferguson");
    	enseignant.setPrenom("Sir");
    	enseignant.setSalaire(10000);
    	System.out.println("Step 3" + enseignant);
    	/* étudiant */
    	Etudiant etudiant = new Etudiant(null, null, null);
    	etudiant.setNom("Mourinho");
    	etudiant.setPrenom("Jose");
    	etudiant.setNiveau("Ligue 1");
    	/* Persistance */
    	System.out.println("Step 4"+ etudiant);
    	Configuration configuration = new Configuration().configure();
    	SessionFactory sessionFactory = configuration.buildSessionFactory();
    	Session session = sessionFactory.openSession();
    	Transaction transaction = session.beginTransaction();
    	session.persist(personne);
    	session.persist(etudiant);
    	session.persist(enseignant);
    	transaction.commit();
    	session.close();
    	sessionFactory.close();
    }
}
