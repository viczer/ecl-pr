package org.eclipse.model;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue(value = "ENS")
public class Enseignant extends Personne {
	private int salaire;

//	public Enseignant(String nom, String prenom ) {
//		super(nom, prenom);
//		// TODO Auto-generated constructor stub
//	}
//
//	public Enseignant(int salaire) {
//		super();
//		this.salaire = salaire;
//	}
	

	public Enseignant(String nom, String prenom,int salaire ) {
		super(nom, prenom);
		this.salaire = salaire;
	}

	

	

	public int getSalaire() {
		return salaire;
	}

	public void setSalaire(int salaire) {
		this.salaire = salaire;
	}

	@Override
	public String toString() {
		return "Enseignant [salaire=" + salaire + "]";
	}

}