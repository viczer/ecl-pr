package org.eclipse.model;

import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;

@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
@DiscriminatorColumn(name = "TYPE_PERSONNE")
@DiscriminatorValue(value = "PERS")
public class Personne {
	private static int cnt;
	@Id
	private int num;
	private String nom;
	private String prenom;

	public Personne() {
		super();

	}

	public Personne(String nom, String prenom) {

		this.num = ++cnt;
		this.nom = nom;
		this.prenom = prenom;
	}

	public int getNum() {
		return num;
	}

	public void setNum() {
		this.num = ++cnt;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	@Override
	public String toString() {
		return "Personne [num=" + num + ", nom=" + nom + ", prenom=" + prenom + "]";
	}

}
