package org.eclipse.main;


import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import org.eclipse.model.Personne;
import org.eclipse.model.Sport;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;


public class App {
	public static void main( String[] args )
    {
           		Personne p1 = new Personne();
        		Personne p2 = new Personne();
        		p1.setNum(1);
        		p1.setNom("Wick");
        		p1.setPrenom("John");
        		p2.setNum(2);
        		p2.setNom("Bob");
        		p2.setPrenom("Joe");        		      		
        		Sport s1 = new Sport();
        		Sport s2 = new Sport();
        		Sport s3 = new Sport();
        		s1.setNom("football");
        		s2.setNom("tennis");
        		s3.setNom("box");
        		s1.setType("collectif");
        		s2.setType("individuel");
        		s3.setType("collectif ou individuel");
        		p1.addSport(s1);
        		p1.addSport(s3);
        		p2.addSport(s1);
        		p2.addSport(s2);
        		p2.addSport(s3);        		        		
        		Configuration configuration = new Configuration().configure();
        		SessionFactory sessionFactory = configuration.buildSessionFactory();
        		Session session = sessionFactory.openSession();
        		Transaction transaction = session.beginTransaction();        		     		
        		session.persist(p1);
        		session.persist(p2);
        		transaction.commit();
        		session.close();
        		sessionFactory.close();    		
        		
        		System.out.println("insertion reussie ");
        		
    }
}
