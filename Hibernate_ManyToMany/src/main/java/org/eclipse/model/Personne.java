package org.eclipse.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

@Entity
public class Personne  {
	@Id
	private int num;
	private String nom;
	private String prenom;
	@ManyToMany(cascade = { CascadeType.PERSIST, CascadeType.REMOVE })
	private List<Sport> sports = new ArrayList<Sport>();

	
	
public Personne() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Personne(int num, String nom, String prenom, List<Sport> sports) {
	super();
	this.num = num;
	this.nom = nom;
	this.prenom = prenom;
	this.sports = sports;
}
	
	

	public int getNum() {
		return num;
	}

	public void setNum(int num) {
		this.num = num;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	// les getters/setters et constructeur
	public List<Sport> getSports() {
		return sports;
	}

	public void setSports(List<Sport> sports) {
		this.sports = sports;
	}

	public boolean addSport(Sport arg0) {
		return sports.add(arg0);
	}

	public boolean removeSport(Object arg0) {
		return sports.remove(arg0);
	}

	@Override
	public String toString() {
		return "Personne [num=" + num + ", nom=" + nom + ", prenom=" + prenom + ", sports=" + sports + "]";
	}
	
	
}