package org.eclipse.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Sport  {
	@Id
	private String nom;
	private String type;
	private static final long serialVersionUID = 1L;

	public Sport() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Sport(String nom, String type) {
		super();
		this.nom = nom;
		this.type = type;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "Sport [nom=" + nom + ", type=" + type + "]";
	}

}
