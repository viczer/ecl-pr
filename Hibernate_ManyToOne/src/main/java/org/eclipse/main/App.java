package org.eclipse.main;



import org.eclipse.model.Adresse;
import org.eclipse.model.Personne;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;


public class App 
{
    public static void main( String[] args )
    {
    	Adresse adresse = new Adresse();
    	adresse.setRue("New York");
    	adresse.setCodePostal("13015");
    	adresse.setVille("Marseille");
    	Personne personne = new Personne();
    	personne.setNum(1);
    	personne.setAdresse(adresse);
    	personne.setNom("Messi");
    	personne.setPrenom("Thiago");
    	Personne personne2 = new Personne();
    	personne2.setNum(2);
    	personne2.setAdresse(adresse);
    	personne2.setNom("Messi");
    	personne2.setPrenom("Leo");
    	Configuration configuration = new Configuration().configure();
    	SessionFactory sessionFactory = configuration.buildSessionFactory();
    	Session session = sessionFactory.openSession();
    	Transaction transaction = session.beginTransaction();
    	session.persist(personne);
    	session.persist(personne2);
    	transaction.commit();
    	session.close();
    	sessionFactory.close();
    }
}
