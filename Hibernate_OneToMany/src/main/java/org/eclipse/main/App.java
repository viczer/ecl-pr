package org.eclipse.main;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.model.Adresse;
import org.eclipse.model.Personne;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

public class App {
	public static void main(String[] args) {
		List <Adresse> adresses = new ArrayList <Adresse> ();
		Adresse a1 = new Adresse();
		a1.setRue("Estaque");
		a1.setCodePostal("13016");
		a1.setVille("Marseille");
		Adresse a2 = new Adresse();
		a2.setRue("Merlan");
		a2.setCodePostal("13013");
		a2.setVille("Marseille");
		Personne p1 = new Personne();
		p1.setNom("Wick");
		p1.setPrenom("John");
		
		adresses.add(a1);
		adresses.add(a2);
		p1.setAdresses(adresses);
		
//		p1.addAdresse(a1);
//		p1.addAdresse(a2);
		Configuration configuration = new Configuration().configure();
		SessionFactory sessionFactory = configuration.buildSessionFactory();
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		session.persist(p1);
		transaction.commit();
		session.close();
		sessionFactory.close();
	}
}
