package org.eclipse.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;


@Entity
public class Personne {
	@Id
	private int num;
	private String nom;
	private String prenom;
	@OneToMany(cascade={CascadeType.PERSIST,CascadeType.REMOVE})
	
	private List <Adresse> adresses = new ArrayList <Adresse> ();
	public Personne() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Personne(int num, String nom, String prenom, List<Adresse> adresses) {
		super();
		this.num = num;
		this.nom = nom;
		this.prenom = prenom;
		this.adresses = adresses;
	}
	public int getNum() {
		return num;
	}
	public void setNum(int num) {
		this.num = num;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	public List<Adresse> getAdresses() {
		return adresses;
	}
	public void setAdresses(List<Adresse> adresses) {
		this.adresses = adresses;
	}
	@Override
	public String toString() {
		return "Personne [num=" + num + ", nom=" + nom + ", prenom=" + prenom + ", adresses=" + adresses + "]";
	}
//	public boolean addAdresse(Adresse a1) {
//		return adresses.add(a1);
//		
//	}

	
	
	
}