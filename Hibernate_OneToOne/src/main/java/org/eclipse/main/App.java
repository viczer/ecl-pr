package org.eclipse.main;

import org.eclipse.model.Adresse;
import org.eclipse.model.Personne;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
		/* Adresse */
    	Adresse adresse = new Adresse();
		adresse.setRue("Lyon");
		adresse.setCodePostal("13015");
		adresse.setVille("Marseille");
		/* Personne */
		Personne personne = new Personne();
		personne.setAdresse(adresse);
		personne.setNom("Ego");
		personne.setPrenom("Paul");
		/* Persistance */
		Configuration configuration = new Configuration().configure();
		SessionFactory sessionFactory = configuration.buildSessionFactory();
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		session.persist(personne);
		transaction.commit();
		session.close();
		sessionFactory.close();
	}
}
