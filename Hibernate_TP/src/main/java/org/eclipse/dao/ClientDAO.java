package org.eclipse.dao;
import org.hibernate.Session;

import com.mysql.fabric.xmlrpc.Client;
public class ClientDAO extends GenericDAO<Client, Integer> {
	public ClientDAO(Session session) {
		super(Client.class, session);
	}
}