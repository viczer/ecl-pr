package org.eclipse.classes;

import java.util.Arrays;
import java.util.Date;

import org.eclipse.models.Employee;
import org.eclipse.models.User;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * Hello world!
 *
 */
public class App {
	public static void main(String[] args) {
		System.out.println("-------------------------------------------");
		User u = new User(1, "Jean", "Baton", "jb@jb.fr");
		Gson gson = new Gson();
		String jsonString = gson.toJson(u);
		System.out.println(jsonString);
		System.out.println("-------------------------------------------");
		String jsonString2 = "{'id': 1, 'firstName':'Bruce', 'lastName': 'Baton', 'email': 'br@baton.fr'}";
		Gson gson2 = new Gson();
		User userObject = gson2.fromJson(jsonString2, User.class);
		System.out.println(userObject);
		System.out.println("---------------------------------------------");

		Employee employee1 = new Employee();
		// convertir un objet en json avec method toJson()

		employee1.setId(1);
		employee1.setFirstName("Bruce");
		employee1.setLastName("Wayne");
		employee1.setRoles(Arrays.asList("ADMIN", "MANAGER"));

		Gson gson3 = new Gson();
		System.out.println(gson3.toJson(employee1));

		System.out.println("--------------------------------------------------");

		Gson gson4 = new Gson();
		System.out.println(gson4.fromJson(
				"{'id': 1, 'firstName':'Bruce', 'lastName': 'Wayne', 'email': 'br@wayne.fr', 'roles': ['ADMIN','MANAGER']}",
				Employee.class));
		System.out.println("--------------------------------------------------");

		GsonBuilder gsonBuilder = new GsonBuilder();
		gsonBuilder.registerTypeAdapter(Date.class, new DateSerializer());
		gsonBuilder.registerTypeAdapter(Date.class, new DateDeserializer());

		Employee employee = new Employee();
		employee.setId(1);
		employee.setFirstName("Clark");
		employee.setLastName("Kent");
		employee.setRoles(Arrays.asList("ADMIN", "MANAGER"));
		employee.setBirthDate(new Date());
		GsonBuilder gsonBuilder2 = new GsonBuilder();
		gsonBuilder2.registerTypeAdapter(Date.class, new DateSerializer());
		gsonBuilder2.registerTypeAdapter(Date.class, new DateDeserializer());
		Gson gson5 = gsonBuilder2.create();
		// Convert to JSON
		System.out.println(gson5.toJson(employee));// Convert to java objects
		System.out.println(gson5.fromJson(
				"{'id':1,'firstName':'Clark','lastName':'Kent',      'roles':['ADMIN','MANAGER'],'birthDate':'12/08/1998'}",
				Employee.class));

	}
}
