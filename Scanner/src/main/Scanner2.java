package main;
import java.util.InputMismatchException;
import java.util.Locale;
import java.util.Scanner;

public class Scanner2 {

	 public static void main( String[] args ) throws InputMismatchException {

	        try ( Scanner scanner = new Scanner( System.in ) ) {
	          
	            scanner.useLocale( Locale.ENGLISH );
	            
	            System.out.print( "Enter an IP address, a double value and an email: " );
	            
	            String ip = scanner.next( "\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}" );
	            System.out.println( "IP address == " + ip );
	            
	            double value = scanner.nextDouble();
	            System.out.println( "double == " + value );

	            String email = scanner.next( "[\\w.-]+@[\\w.-]+\\.[a-z]{2,}" );
	            System.out.println( "email == " + email );
	        }
	        
	    }


}
