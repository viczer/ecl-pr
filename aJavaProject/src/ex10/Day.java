//�crivez un programme Java qui donne le jour de la semaine.

package ex10;
import java.util.*;

public class Day {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.print("Input number:");
		int day = sc.nextInt();
		
		System.out.print(getDayName(day));
		
		sc.close();		
		
	}
	
	public static String getDayName(int day) {
		String dayName = "";
        switch (day) {
            case 1: dayName = "Monday"; break;
            case 2: dayName = "Tuesday"; break;
            case 3: dayName = "Wednesday"; break;
            case 4: dayName = "Thursday"; break;
            case 5: dayName = "Friday"; break;
            case 6: dayName = "Saturday"; break;
            case 7: dayName = "Sunday"; break;
            default:dayName = "Invalid day range";
        }
        return (dayName);
		
	}

}



