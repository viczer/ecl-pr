/** �crire un programme Java pour convertir des minutes en un certain nombre d'ann�es et de jours
 * 1440 min x giorno*/
package ex10;
import java.util.*;

public class Minut {

	public static void main(String[] args) {
		
		double minYear = 24 * 60 * 365;
		Scanner sc = new Scanner(System.in);
		System.out.print("Input the number of minutes: ");
		int min = sc.nextInt();
		
		long years = (long) (min / minYear);
		int days = (int) (min / 60 / 24) % 365;

        System.out.println((int) min + " minutes is  " + years + " years and " + days + " days");
		
		
		sc.close();
		
	}

}
