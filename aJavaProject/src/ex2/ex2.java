/**�crivez un programme Java qui demande un chiifre (RAYON) � l�utilisateur et qui affiche
l'aire et le p�rim�tre d'un cercle*/
package ex2;

import java.util.Scanner;

public class ex2 {

	public static void main(String[] args) {

		try (Scanner scanner = new Scanner(System.in)) {

			System.out.print("rayon : ");
			int r = scanner.nextInt();
			double ar = Math.PI * r * r;
			double pr = 2 * Math.PI * r;

			System.out.printf("l'aire d'un cercle " + ar);
			System.out.printf(" et le p�rim�tre d'un cercle " + pr);

		}

	}
}


  