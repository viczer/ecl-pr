package ex4;

import java.util.Scanner;

public class pariDispari {

	public static void main(String[] args) {

		try (Scanner scanner = new Scanner(System.in)) {

			System.out.print("Scegliere un numero intero : ");
			int a = scanner.nextInt();

			
			if ((a % 2) == 0) {
				System.out.print("Numero � pari ");
			} else {
				System.out.print("Numero � dispari ");
			}
			
			
		}

	}

}