package ex6Switch;

public class Switch {

	public static void main(String[] args) {
		int x=5;
		switch (x) {
		case 1:
			System.out.println("Corsica");
			break;
		case 2:
			System.out.println("Creta");
			break;
		case 3:
			System.out.println("Mikonos");
			break;
		case 4:
			System.out.println("Ibiza");
			break;
		default:
			System.out.println("altro");
			break;
			
		}
	}

}
