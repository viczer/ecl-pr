package ex7;

import java.util.*;

public class Destinazione {

	public static void main(String[] args) {
	
		
			try (Scanner scanner = new Scanner(System.in)) {
				
				System.out.print("Scegliere una destinazione:"
						+ "\ndigitare 1 per Corsica, "
						+ "\ndigitare 2 per Creta, "
						+ "\ndigitare 3 per Mikonos"
						+ "\ndigitare 4  per Ibiza: \n ");
				int d = scanner.nextInt();

				switch (d) {
				case 1:
					System.out.println("Corsica");
					break;
				case 2:
					System.out.println("Creta");
					break;
				case 3:
					System.out.println("Mikonos");
					break;
				case 4:
					System.out.println("Ibiza");
					break;
				default:
					System.out.println("altro");
					break;
				}
				
				
			}
	}
}

	
