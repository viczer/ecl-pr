package main;

import java.util.Scanner;

public class MyClass {

	public static void main(String[] args) {

		try (Scanner scanner = new Scanner(System.in)) {

			System.out.print("Veuillez saisir un premier entier : ");
			int a = scanner.nextInt();

			System.out.print("Veuillez saisir un second entier : ");
			int b = scanner.nextInt();
			int result1 = a + b;
			int result2 = a * b;
			//double result3 = a/b;
			
			System.out.printf("La somme de %d et de %d vaut %d\n", a, b, result1);
			System.out.printf("La moltiplication de %d et de %d vaut %d\n", a, b, result2);
			//System.out.printf("La division de %d et de %d vaut %d\n", a, b, result3);

		}

	}

}