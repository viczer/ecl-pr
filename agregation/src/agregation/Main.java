package agregation;

public class Main {

	public static void main(String[] args) {
		Chassi chassi = new Chassi(1, "Fierst");
		//chassi.setId(100);
		//chassi.setMatiere("fer");
		Moteur moteur = new Moteur(1, 150);
		Vehicule vehicule = new Vehicule("BJ552JN", "Peugeot", "206", moteur, chassi);
		moteur.setNum(100);
		moteur.setPoids(500f);
		vehicule.setMoteur(moteur);
		// pas de m�thode permettant de modifier le ch�ssis d�un v�hicule
		
		System.out.println(vehicule);

	}

}
