package agregation;

public class Vehicule {
	private String numImmat;
	private String marque;
	private String modele;
	
	private Moteur moteur;
	private final Chassi chassi;
	
	public Vehicule(String numImmat, String marque, String modele, Moteur moteur, Chassi chassi) {
		super();
		this.numImmat = numImmat;
		this.marque = marque;
		this.modele = modele;
		this.moteur = moteur;
		this.chassi = chassi;
	}

	public String getNumImmat() {
		return numImmat;
	}

	public void setNumImmat(String numImmat) {
		this.numImmat = numImmat;
	}

	public String getMarque() {
		return marque;
	}

	public void setMarque(String marque) {
		this.marque = marque;
	}

	public String getModele() {
		return modele;
	}

	public void setModele(String modele) {
		this.modele = modele;
	}
//**************************************************
	public Moteur getMoteur() {
		return moteur;
	}

	public void setMoteur(Moteur moteur) {
		this.moteur = moteur;
	}

	public Chassi getChassi() {
		return chassi;
	}
//**************************************************
	@Override
	public String toString() {
		return "Vehicule [numImmat=" + numImmat + ", marque=" + marque + ", modele=" + modele + ", moteur=" + moteur
				+ ", chassi=" + chassi + "]";
	}
	

}
