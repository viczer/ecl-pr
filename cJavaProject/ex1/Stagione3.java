package ex1;

import java.util.Scanner;

public class Stagione3 {
	public static void stagione(int mese) {
						
			if((mese<=2)&(mese==12)){
				System.out.print("Inverno  ");
			}else if ((mese>=3)&(mese<=5)) {
				System.out.print("Primavera ");
			}else if ((mese>=6)&(mese<=8)) {
				System.out.print("Estate ");
			}else {
				System.out.print("Autunno ");
			}
		
	}

	public static void main(String[] args) {
		
	
		try (Scanner scanner = new Scanner(System.in)) {

			System.out.print("Digitare il numero di mese : ");
			int mese = scanner.nextInt();
			stagione(mese);
			scanner.close();

	}
		

}
}
