package arraylist;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.ListIterator;

public class MethUtil {
	public static void main(String[] args) {
		List<String> lettres = new ArrayList<String>();
		lettres.add("d");
		lettres.add("b");
		lettres.add("a");
		lettres.add("c");
		Collections.sort(lettres); // pour trier la liste
		System.out.println(lettres);
		Collections.shuffle(lettres); // pour desordonner la liste
		System.out.println(lettres);
		List<String> sub = lettres.subList(1, 2); // extraire une sous-liste
		System.out.println(sub);
		Collections.reverse(sub); // pour trier la liste dans le sens decroissant
		System.out.println(sub);
		Collections.reverse(lettres);
		System.out.println(lettres);
		
		}

}
