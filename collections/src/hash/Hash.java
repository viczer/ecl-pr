package hash;
import java.util.HashMap;
import java.util.Map.Entry;

public class Hash {
	
	
	public static void main(String[] args) {
		
		System.out.println("**********HashMap****************************");
		HashMap<Integer, String> hm = new HashMap();
		hm.put(1, "Java");
		hm.put(2, "PHP");
		hm.put(10, "C++");
		hm.put(17, null);
		for (Entry<Integer, String> entry : hm.entrySet()) {
		
		System.out.print("\n"+entry.getKey() + " " + entry.getValue()+"\t");
		}
		}

}
