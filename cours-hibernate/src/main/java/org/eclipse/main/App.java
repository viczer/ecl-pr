package org.eclipse.main;



import org.eclipse.model.Personnes;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

public class App {
	public static void main(String[] args) {
		Personnes personnes = new Personnes();
		personnes.setNom("travolta");
		personnes.setPrenom("john");
		Configuration configuration = new Configuration().configure();
		SessionFactory sessionFactory = configuration.buildSessionFactory();
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		session.persist(personnes);
		transaction.commit();
		session.close();
		sessionFactory.close();
	}
}
