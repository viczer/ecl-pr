<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Ajout</title>
</head>
<body>
	<form method="post" action="ajoutPersonne">
		<div>Formulaire d'ajout d'une Personne</div>
		<div>
			<label for="nom">Nom*</label> <input type="text" id="nom" name="nom"
				value="${ nomSaisi }" />${ nomIncorrect } <span class="erreur">${form.erreurs['nom']}</span>
		</div>
		<div>
			<label for="prenom">Prenom*</label> <input type="text" id="prenom"
				name="prenom" value="${ prenomSaisi }" />${ prenomIncorrect } <span
				class="erreur">${form.erreurs['prenom']}</span> 
				<!-- <input type="submit" value="Ajouter" />-->
		</div>
		<input type="submit" value="Ajouter" />
	</form>
</body>
</html>