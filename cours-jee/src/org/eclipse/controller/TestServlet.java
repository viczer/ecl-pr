package org.eclipse.controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.models.Personne;

/**
 * Servlet implementation class TestServlet
 */
@WebServlet("/mapage")
public class TestServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
//    public TestServlet() {
//        super();
//        // TODO Auto-generated constructor stub
//    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//ServletOutputStream out = response.getOutputStream();
		 
//        out.println("<style> span {color:blue;} </style>");
//		
//		ArrayList<String> sport = new ArrayList<String>();
//		sport.add("football");
//		sport.add("tennis");
//		sport.add("rugby");
//		sport.add("basketball");
//		request.setAttribute("sport", sport);
//
		Personne perso = new Personne();
		perso.setNum(100);
		perso.setNom("Wick");
		perso.setPrenom("John");
		request.setAttribute("perso",perso);
//		
		
//		String ville = "Marseille";
//		request.setAttribute("maVille",ville);
//		
		this.getServletContext().getRequestDispatcher("/WEB-INF/vue.jsp").forward(request,response);
	
//		String nom = request.getParameter("nom");
//		String prenom = request.getParameter("prenom");
//		PrintWriter out = response.getWriter();
//		out.print("Hello " + nom + " " + prenom);
		
		
		
		//	response.setContentType("text/html");
//		response.setCharacterEncoding("UTF-8");
//		PrintWriter out = response.getWriter();
//		out.println("<!DOCTYPE html>");
//		out.println("<html>");
//		out.println("<head>");
//		out.println("<meta charset=\"utf-8\"/>");
//		out.println("<title>Project JEE</title>");
//		out.println("</head>");
//		out.println("<body>");
//		out.println("Hello World");
//		out.println("</body>");
//		out.println("<html>");
		
		//response.getWriter().print("Hello World");
		
		
//		
//		 // Client Infos
//        out.println("<br><br><b>Client info:</b>");
// 
//        out.println("<br><span>remoteAddr:</span>");
//        String remoteAddr = request.getRemoteAddr();
//        out.println(remoteAddr);
// 
//        out.println("<br><span>remoteHost:</span>");
//        String remoteHost = request.getRemoteHost();
//        out.println(remoteHost);
// 
//        out.println("<br><span>remoteHost:</span>");
//        int remotePort = request.getRemotePort();
//        out.println(remotePort + "");
// 
//        out.println("<br><span>remoteUser:</span>");
//        String remoteUser = request.getRemoteUser();
//        out.println(remoteUser);
	}
	
	
	
	

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
