package org.eclipse.controller;

import org.eclipse.forms.AjoutPersonneForm;
import org.eclipse.models.*;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.models.Personne;
import org.eclipse.service.PersonneDaoImpl;

/**
 * Servlet implementation class ajoutPersonne
 */
@WebServlet("/ajoutPersonne")
public class ajoutPersonneServlet extends HttpServlet {
	public static final String ATT_PERSONNE = "personne";
	public static final String ATT_FORM = "form";
	public static final String VUE_SUCCES = "/WEB-INF/confirmation.jsp";
	public static final String VUE_FORM = "/WEB-INF/ajoutPersonne.jsp";
	
	private static final long serialVersionUID = 1L;

	private PersonneDaoImpl personneDao;
	public void init() {
		personneDao = new PersonneDaoImpl();
	}
	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ajoutPersonneServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
//		// TODO Auto-generated method stub
//		response.getWriter().append("Served at: ").append(request.getContextPath());
		this.getServletContext().getRequestDispatcher("/WEB-INF/ajoutPersonne.jsp").forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		/* Préparation de l'objet formulaire */
		AjoutPersonneForm form = new AjoutPersonneForm();
		/* Traitement de la requête et récupération du bean en résultant */
		Personne personne = form.creerPersonne(request);
		/* Ajout du bean et de l'objet métier à l'objet requête */
		request.setAttribute(ATT_PERSONNE, personne);
		request.setAttribute(ATT_FORM, form);

		if (form.getErreurs().isEmpty()) {
		
			PersonneDaoImpl daop = new PersonneDaoImpl();
			Personne insertedPersonne = daop.save(personne);
			request.setAttribute("personne", insertedPersonne);

			/* Si aucune erreur, alors affichage de la fiche récapitulative */
			this.getServletContext().getRequestDispatcher(VUE_SUCCES).forward(request, response);
		} else {
			/* Sinon, ré-affichage du formulaire de création avec les erreurs */
			this.getServletContext().getRequestDispatcher(VUE_FORM).forward(request, response);
		}
	}


//
//		boolean test = true;
//		String nom = request.getParameter("nom");
//		String prenom = request.getParameter("prenom");
//		try {
//			verifChaine(nom);
//		} catch (Exception e) {
//			request.setAttribute("nomIncorrect", e.getMessage());
//			test = false;
//		} 
//		try {
//			verifChaine(prenom);
//		} catch (Exception e) {
//			request.setAttribute("nomIncorrect", e.getMessage());
//			test = false;
//		} 
//		
//		
//		if (test) {
//			Personne personne = new Personne(0, nom, prenom);
//			PersonneDaoImpl daop = new PersonneDaoImpl();
//			Personne insertedPersonne = daop.save(personne);
//			request.setAttribute("personne", insertedPersonne);
//		
//			this.getServletContext().getRequestDispatcher("/WEB-INF/confirmation.jsp").forward(request, response);
//		} else {
//		
//			request.setAttribute("nomSaisi", nom);
//			request.setAttribute("prenomSaisi", prenom);
//
//			
//
//			this.getServletContext().getRequestDispatcher("/WEB-INF/ajoutPersonne.jsp").forward(request, response);
//
//		}
//
//	}
//
//	public void verifChaine(String s)throws Exception{
//		if (s == null || s.length() < 2)
//			throw new Exception("La chaıne doit comporter aumoins deux caract`eres");
//			
//			
//		char c = s.charAt(0);
//		if (!(c >= 'A' && c <= 'Z'))
//			
//			throw new Exception("La chaıne doit commencer par unelettre en majuscule");
//		
//		for (int i = 0; i < s.length(); i++) {
//			c = s.charAt(i);
//			if (!(c >= 'a' && c <= 'z') && !(c >= 'A' && c <= 'Z'))
//				throw new Exception("La chaıne ne peut contenir quedes lettres");
//		}
//		
}

