package org.eclipse.service;

import org.eclipse.models.Personne;

public interface DAO <T> {
	void saveAndShow(T o);
	void removeAndShow(T o);
	void updateAndShow(T o);
	Personne save(T o);
}