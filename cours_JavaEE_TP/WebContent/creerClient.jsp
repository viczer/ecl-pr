<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="org.tp.beeans.*"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Cr�ation d'un client</title>
</head>
<header>
	<%@ include file="inc/menu.jsp"%>
</header>

<body>
	<%@ include file="inc/client_form.jsp"%>
</body>
</html>