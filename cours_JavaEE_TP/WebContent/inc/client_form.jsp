<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
	<%@ taglib uri  = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ page import = "org.tp.beeans.*" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Form client</title>
<link type="text/css" rel="stylesheet" href="inc/style.css" />
</head>


<body>
	<div>
		<form method="get" action="creationClient">
			<fieldset>
				<legend>Informations client</legend>
				<label for="nomClient">Nom <span class="requis">*</span></label> <input
					type="text" id="nomClient" name="nomClient" value="" size="20"
					maxlength="20" /> <br /> <label for="prenomClient">Prenom
				</label> <input type="text" id="prenomClient" name="prenomClient" value=""
					size="20" maxlength="20" /> 
					
					
					<br /> <label for="telephoneClient">Num�ro
					de t�l�phone <span class="requis">*</span>
				</label> <input type="text" id="telephoneClient" name="telephoneClient"
					value="" size="20" maxlength="20" /> <br /> <label
					for="emailClient">Adresse email</label> <input type="email"
					id="emailClient" name="emailClient" value="" size="20"
					maxlength="60" /> <br />
			</fieldset>
			<input type="submit" value="Valider" /> <input type="reset"
				value="Remettre � z�ro" /> <br />
		</form>
	</div>
</body>
</html>