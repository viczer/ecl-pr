<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>



<%@ page import = "org.tp.beeans.*" %>
    
    
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>MENU</title>
<link type="text/css" rel="stylesheet" href="inc/style.css" />
</head>
<body>

<c:url value="/" var="homeUrl"/>
<c:url value="user" var="userUrl"/>
<c:url value="admin" var="adminUrl"/>
<c:url value="logout" var="logoutUrl"/>

<div class="menu">
	<ul id="navigation">
		<p><c:url value ="/creerClient.jsp" var ="client"/></p>
		<li><a href="${client}">NOUVEAU CLIENT</a></li>
		
		<p><c:url value ="/creerAdress.jsp" var ="adress"/></p>
		<li><a href="${adress}">NOUVEAU ADRESSE</a></li>
		
		
	</ul>
	
	<br style="clear:left"/>
</div>

</body>
</html>
