package org.tp.beeans;

public class Adress {
	private Client client;
	private String ville;
	private String codePostal;
	private String rue;
	public Adress() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Adress(Client client, String ville, String codePostal, String rue) {
		super();
		this.client = client;
		this.ville = ville;
		this.codePostal = codePostal;
		this.rue = rue;
	}
	public Client getClient() {
		return client;
	}
	public void setClient(Client client) {
		this.client = client;
	}
	public String getVille() {
		return ville;
	}
	public void setVille(String ville) {
		this.ville = ville;
	}
	public String getCodePostal() {
		return codePostal;
	}
	public void setCodePostal(String codePostal) {
		this.codePostal = codePostal;
	}
	public String getRue() {
		return rue;
	}
	public void setRue(String rue) {
		this.rue = rue;
	}
	@Override
	public String toString() {
		return "Adress [client=" + client + ", ville=" + ville + ", codePostal=" + codePostal + ", rue=" + rue + "]";
	}
	
	
}
