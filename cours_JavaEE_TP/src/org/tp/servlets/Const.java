package org.tp.servlets;

public class Const {
	public static final String CHAMP_NOM = "nom";
	public static final String CHAMP_PRENOM = "prenom";
	public static final String CHAMP_TEL = "telephone";
	public static final String CHAMP_MAIL = "email";
	public static final String CHAMP_VILLE = "Ville";
	public static final String CHAMP_CODEPOSTAL = "codePostal";
	public static final String CHAMP_RUE = "Rue";
	public static final String VUE_CLIENT = "/afficherClient.jsp";
	public static final String VUE_ADR = "/404.jsp";
	
	
}
