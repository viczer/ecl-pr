package org.tp.servlets;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.tp.beeans.Adress;
import org.tp.beeans.Client;
import org.tp.beeans.*;

/**
 * Servlet implementation class creationAdress
 */
@WebServlet("/creationAdress")
public class creationAdress extends HttpServlet {
	private static final long serialVersionUID = 1L;
	public static ArrayList<Adress> listeDeAdresse= new ArrayList<Adress>();
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public creationAdress() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		this.getServletContext().getRequestDispatcher("/creerAdress.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String nom = request.getParameter(Const.CHAMP_NOM );
		String prenom = request.getParameter("prenom");
		String telephone = request.getParameter("telephone");
		String email = request.getParameter("email");
		Client client = new Client(nom, prenom, telephone, email );
		String ville = request.getParameter("Ville");
		String codePostal = request.getParameter("codePostal");	
		
		String rue = request.getParameter("Rue");
		
		Adress adresse = new Adress(client, ville,  codePostal, rue);
		listeDeAdresse.add(adresse);
		request.setAttribute("adresse", adresse);
		this.getServletContext().getRequestDispatcher("/afficherAdress.jsp").forward(request, response);
	}

}
