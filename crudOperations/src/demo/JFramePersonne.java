package demo;

import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import net.proteanit.sql.DbUtils;
import services.MyConnection;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.awt.event.ActionEvent;
import javax.swing.JTable;
import javax.swing.JScrollPane;
import javax.swing.JTextField;

public class JFramePersonne extends JFrame {

	private JPanel contentPane;
	private JTable table;
	private JTextField textNum;
	private JTextField textNom;
	private JTextField textPrenom;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					JFramePersonne frame = new JFramePersonne();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public JFramePersonne() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 11, 214, 205);
		contentPane.add(scrollPane);

		table = new JTable();
		scrollPane.setViewportView(table);

		JLabel lblNum = new JLabel("Num");
		lblNum.setBounds(251, 53, 48, 14);
		contentPane.add(lblNum);

		textNum = new JTextField();
		textNum.setBounds(309, 50, 96, 20);
		contentPane.add(textNum);
		textNum.setColumns(10);

		JLabel lblNom = new JLabel("Nom");
		lblNom.setBounds(251, 84, 48, 14);
		contentPane.add(lblNom);

		JLabel lblPrenom = new JLabel("Prenom");
		lblPrenom.setBounds(251, 120, 48, 14);
		contentPane.add(lblPrenom);

		textNom = new JTextField();
		textNom.setColumns(10);
		textNom.setBounds(309, 81, 96, 20);
		contentPane.add(textNom);

		textPrenom = new JTextField();
		textPrenom.setColumns(10);
		textPrenom.setBounds(309, 114, 96, 20);
		contentPane.add(textPrenom);
		/*** button */

		JButton Select = new JButton("Select");
		Select.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				fillData();
			}
		});
		Select.setBounds(10, 227, 89, 23);
		contentPane.add(Select);

		JButton btnUpdate = new JButton("Update");
		btnUpdate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				update();
			}
		});
		btnUpdate.setBounds(230, 227, 89, 23);
		contentPane.add(btnUpdate);

		JButton btnInsert = new JButton("Insert");
		btnInsert.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				insert();
			}
		});
		btnInsert.setBounds(123, 227, 89, 23);
		contentPane.add(btnInsert);

		JButton btnDelete = new JButton("Delete");
		btnDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				delete();
			}
		});
		btnDelete.setBounds(335, 227, 89, 23);
		contentPane.add(btnDelete);
	}

	// METODI
	public void fillData() {
		Connection c = MyConnection.getConnection();
		try {
			String sql = "select*from personne";
			PreparedStatement pst = c.prepareStatement(sql);
			ResultSet rs = pst.executeQuery();
			table.setModel(DbUtils.resultSetToTableModel(rs));

		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public void insert() {
		Connection c = MyConnection.getConnection();
		try {
			String sql = "INSERT INTO personne" + "(nom,prenom)" + "VALUES ('" + textNom.getText() + "','"
					+ textPrenom.getText() + "') ";
			PreparedStatement pst = c.prepareStatement(sql);
			int rs = pst.executeUpdate();
			textNum.setText("");
			textNom.setText("");
			textPrenom.setText("");
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	private void delete() {
		Connection c = MyConnection.getConnection();
		try {
			String sql = "DELETE FROM personne where num = ?";
			PreparedStatement pst = c.prepareStatement(sql);
			pst.setInt(1, Integer.parseInt(textNum.getText()));
			int rs = pst.executeUpdate();
			if (rs == 1) {
				JOptionPane.showMessageDialog(null, "Suppression r�alis�e");
				textNum.setText("");
				fillData();
			}

		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public void update() {
		Connection c = MyConnection.getConnection();
		try {
			String sql = "update personne " + "set nom = '" + textNom.getText() + "', prenom ='" + textPrenom.getText()
					+ "'" + "where num = '" + textNum.getText() + "' ";
			PreparedStatement pst = c.prepareStatement(sql);
			int rs = pst.executeUpdate();
			textNum.setText("");
			textNom.setText("");
			textPrenom.setText("");
			JOptionPane.showMessageDialog(null, "tuple mis � jour !!!!");
			fillData();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
}
