package ex3;

import java.util.Scanner;

public class ex3 {

	public static void main(String[] args) {
//		TP 1  
//		Ecrire un programme qui permet un utilisateur de calculer 
//		le prix de la connexion internet dans un cyber-cafe. 	 
//		Le programme demandera � l'utilisateur d'entrer l'heure � laquelle il s'est connect� 
//		et l'heure � laquelle il s'est d�connect�. 
//		Vous pouvez faire les simplifications suivantes:  
//			� Il n'est pas n�cessaire d'indiquer les minutes. 
//			On calcule donc avec des heures enti�res. => non
//			� L'heure du d�but de la connexion est toujours inf�rieure � l'heure de la fin de la connexion. 
//			Cela veut donc dire que l'utilisateur n'est jamais connect� de avant minuit 
//			jusqu'� apr�s minuit => non
//			et que la dur�e maximale d'une connexion est de 23 heures. => oui
//		Les tarifs de connexion sont les suivants:  
//			� Tarif 1 de 7h � 17h : 1�/heure 
//			� Tarif 2 de 0h � 7h et de 17h � 24h : 2 �/heure 
//		Modularisez le programme sous forme de m�thodes.
		
		//tableau cout
		int[] tableauCoutX = {0,420,1020,1440};
		int[] tableauCoutY = {0,14,24,38};
			
		int debutM = saisieHeure(true);
		int finM = saisieHeure(false);
		float minute = calculCout((float)debutM, (float)finM, 1440);
		int heure = (int)minute/60;
		minute %= 60;
		
		float coutDebut = interPopol(tableauCoutX, tableauCoutY, debutM);
		float coutFin = interPopol(tableauCoutX, tableauCoutY, finM);
		float cout = calculCout(coutDebut, coutFin, 38);
		
		System.out.println("La dur�e de la connexion est de " + heure + " heures et " + minute + " minutes");
		System.out.println("Le cout de la connexion est de : " + cout + "�");
	}
	
	static int saisieHeure(boolean debut) 
	{
		int minute, heure;
		String message;
		
		Scanner monScanner = new Scanner(System.in);
		
		if (debut) {		
			message = "debut";
		} else {
			message = "fin";
		}
		
		System.out.println("Veuillez renseigner l'heure de " + message + " de connexion,");
		System.out.println("sous le format HH MM : ");
		heure = monScanner.nextInt();
		minute = monScanner.nextInt();
		monScanner.nextLine(); // pour vider le scanner

		if (!debut) {
			monScanner.close();
		}
		return (heure * 60 + minute);
	}
	
	static float interPopol(int[] tabX, int[] tabY, int valX) {
		int indX = 0;
		float ratio, ratioa, ratiob, result;
		
		while (valX > tabX[(indX + 1)]) {
			indX++;
		}
		
		ratioa = (float)(valX - tabX[indX]);
		ratiob = (float)((tabX[(indX + 1)] - tabX[indX]));
		ratio = ratioa/ratiob;
		
		result = (float)tabY[indX] * (1-ratio) + (float)tabY[(indX + 1)] * (ratio);
			
		return(result);
	}
	
	static float calculCout(float cDeb, float cFin, float cMax) {
		float cout;
		if (cDeb <= cFin) { 	// cas classique
			cout = cFin - cDeb;
		} else {			// changement de jour
			cout = (cMax - cDeb) + cFin;
		}
		return(cout);
	}

}