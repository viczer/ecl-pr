package ma.tp.ecole;
import java.text.SimpleDateFormat;
import java.util.*;

public class Etudiant {
	private int id;
	private String nom;
	private String prenom;
	private Date dateNais;
	private Filiere filiere;
	
	private static int count;

	
	public Etudiant(String nom, String prenom, Date dateNais, Filiere filiere) {
		super();
		this.id = ++count;
		this.nom = nom;
		this.prenom = prenom;
		this.dateNais = dateNais;
		this.filiere = filiere;
	}

	public Etudiant() {
		super();
		
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	

	public Date getDateNais() {
		return dateNais;
	}

	public void setDateNais(Date dateNais) {
		this.dateNais = dateNais;
	}

	@Override
	public String toString() {
		SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		String date = df.format(dateNais);
		return "Je suis l'�tudiant " + this.nom + " " + this.prenom
				+ " ma date de naissance est : "
				+ df.format(this.dateNais);

	}

	public Etudiant getFiliere() {
		// TODO Auto-generated method stub
		return null;
	}
	
	
	
	
	

}
