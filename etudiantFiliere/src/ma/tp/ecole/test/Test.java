package ma.tp.ecole.test;
import java.util.*;

import ma.tp.ecole.Etudiant;
import ma.tp.ecole.Filiere;

public class Test {

	public static void main(String[] args) {
		Filiere filieres[] = new Filiere[3];
		filieres[0] = new Filiere("Info 2",  "Informatique");
		filieres[1] = new Filiere("TIC",  "Techno de l'info et de communication");
		filieres[2] = new Filiere("GC",  "Genie Civil");
		
		Etudiant etudiants[] = new Etudiant[5];
		etudiants[0]=new Etudiant( "Jean",  "Pat",  new Date("1980/05/05"),  filieres[1]);
		etudiants[1]=new Etudiant("Jul",  "Dre", new Date("1980/05/05"),  filieres[2]);
		etudiants[2]=new Etudiant("Dom", "minique",  new Date("1980/05/05"),  filieres[0]);
		etudiants[3]=new Etudiant("Pat", "Pot",  new Date("1980/05/05"),  filieres[2]);
		etudiants[4]=new Etudiant("Aya",  "YAr",  new Date("1980/05/05"),  filieres[1]);
		
		for (Filiere filiere : filieres) { 
			System.out.println(filiere); 
			for (Etudiant etudiant : etudiants) { 
				if (etudiant.getFiliere().getId() == filiere.getId()) {System.out.println(etudiant);
	}

}}}}

