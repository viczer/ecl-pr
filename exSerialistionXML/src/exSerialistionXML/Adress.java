package exSerialistionXML;

import java.io.Serializable;

public class Adress implements Serializable{

	public String codePostal;
	public String str;
	public String ville;
	private Personne personne;
	
	
	
	
	public Adress(String codePostal, String str, String ville, Personne personne) {
		super();
		this.codePostal = codePostal;
		this.str = str;
		this.ville = ville;
		this.personne = personne;
	}
	public Adress() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public String getCodePostal() {
		return codePostal;
	}
	public void setCodePostal(String codePostal) {
		this.codePostal = codePostal;
	}
	public String getStr() {
		return str;
	}
	public void setStr(String str) {
		this.str = str;
	}
	public String getVille() {
		return ville;
	}

	public Personne getPersonne() {
		return personne;
	}
	public void setPersonne(Personne personne) {
		this.personne = personne;
	}
	@Override
	public String toString() {
		return "Adress [codePostal=" + codePostal + ", str=" + str + ", ville=" + ville + ", personne=" + personne
				+ "]";
	}
	public void setVille(String ville2) {
		// TODO Auto-generated method stub
		
	}
	
	
}
