package exSerialistionXML;
import exSerialistionXML.Adress;


import java.util.*;

public class MainTest {

	public static void main(String[] args) {
		Adress adr1 = new Adress();

		adr1.setCodePostal("111111");
		adr1.setStr("111111");
		adr1.setVille("111111");

		Adress adr2 = new Adress();

		adr2.setCodePostal("22222");
		adr2.setStr("22222222");
		adr2.setVille("2222222");

		ArrayList arrayList1 = new ArrayList();
		arrayList1.add(adr1);
		arrayList1.add(adr2);
		
		Personne personne = new Personne("Viks", "Viks", arrayList1 );		

		try {
			XMLTools.encodeToFile(personne, "personne.xml");
			System.out.println(personne);
			personne = (Personne) XMLTools.decodeFromFile("personne.xml");
			System.out.println(personne);
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

}

