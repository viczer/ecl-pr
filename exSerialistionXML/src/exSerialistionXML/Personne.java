package exSerialistionXML;
import java.util.*;

public class Personne {
	private String nom;
	private String prenom;
	private ArrayList<String> adress;
	public Personne() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Personne(String nom, String prenom, ArrayList<String> adress) {
		
		this.nom = nom;
		this.prenom = prenom;
		this.adress = adress;
	}

	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	public ArrayList<String> getAdress() {
		return adress;
	}
	public void setAdress(ArrayList<String> adress) {
		this.adress = adress;
	}
	@Override
	public String toString() {
		return "Personne [nom=" + nom + ", prenom=" + prenom + ", adress=" + adress + "]";
	}
	
	
}
