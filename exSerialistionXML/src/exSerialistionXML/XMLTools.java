package exSerialistionXML;

import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.FileOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public final class XMLTools {

	public static void encodeToFile(Object object, String fileName) throws FileNotFoundException, IOException {
		// ouverture de l'encodeur vers le fichier
		XMLEncoder encoder = new XMLEncoder(new FileOutputStream(fileName));
		try {
		
			encoder.writeObject(object);
			encoder.flush();
		} finally {

			encoder.close();
		}
	}
	
	public static Object decodeFromFile(String fileName) throws FileNotFoundException, IOException {
		Object object = null;

		XMLDecoder decoder = new XMLDecoder(new FileInputStream(fileName));
		try {

			object = decoder.readObject();
		} finally {

			decoder.close();
		}

		return object;

	}
}