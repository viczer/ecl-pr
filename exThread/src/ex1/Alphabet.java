package ex1;

class Alphabet implements Runnable {

	@Override
	public void run() {

		for (char i = 'a'; i <= 'z'; i++)
			System.out.println(i + " ");
		try {
			Thread.sleep(100);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		Runnable t1 = new Alphabet ();
		Runnable t2 = new Alphabet ();
		new Thread (t1 ). start ();
		new Thread (t2 ). start ();

	}
}
