package ex2;

public class MaTache implements Runnable {

	@Override
	public void run() {
		for(int i = 0; i<25; i++) {
			String nom = Thread.currentThread().getName();
			System.out.println(nom + "est en cours d'esecution");
		}
	}

	public static void main(String[] args) {
		Runnable tache = new MaTache();
		Thread a = new Thread(tache);
		a.setName("Caroline");
		Thread b = new Thread(tache);
		b.setName("Cedric");
		
		a.start();
		b.start();
	}

}
