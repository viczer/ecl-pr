package tp1;

import ex2.MaTache;

public class MainTroisThread implements Runnable {
	@Override
	public void run() {
		for (int i = 0; i < 10; i++) {

			try {
				Thread.currentThread().sleep((int) Math.random() * 200);
			} catch (InterruptedException e) {
				return;
			}
			System.out.println(Thread.currentThread().getName() + " in execution ");
		}
	}

	public static void main(String[] args) {
		Runnable tache = new MainTroisThread();
		Thread a = new Thread(tache);
		a.setName("Caroline");
		Thread b = new Thread(tache);
		b.setName("Cedric");
		Thread c = new Thread(tache);
		c.setName("Jenni");
		a.start();
		b.start();
		c.start();

		try {
			a.join();
			b.join();
			c.join();
		} catch (InterruptedException e) {
			return;
		}
		System.out.println();
		System.out.println();

	}

}

