package exception2;



public class AdressReplaceExcp {
	private String rue;
	private String ville;
	private String codePostal;
	
	
	public AdressReplaceExcp(String rue, String ville, String codePostal)throws IncorrectAdressExcp {
		
		if ((codePostal.length() != 5)|(rue.toUpperCase() != rue))
			throw new IncorrectAdressExcp(codePostal, rue);
		
		this.rue = rue;
		this.ville = ville;
		this.codePostal = codePostal;
	}
	
	
}
