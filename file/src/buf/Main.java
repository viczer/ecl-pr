package buf;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class Main {

	public static void main(String[] args) {
		try {
			File file = new File("fichier1.txt");
			FileWriter fw = new FileWriter(file);
			BufferedWriter bw = new BufferedWriter(fw);
			bw.write("Hello world");
			bw.newLine();
			bw.write(87);
			bw.write('a');
			bw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}
