package ex;

import java.io.*;
import java.util.*;

public class Main {

	public static void main(String[] args) throws IOException {
		  // The reader and writer objects must be declared BEFORE
        // the try block, otherwise they are not 'visible' to close
        // in the finally block
        Scanner reader = null;
        FileWriter writer = null;
        String inputText;

        try {

            // Reader and writer are instantiated within the try block,
            // because they can already throw an IOException
            reader = new Scanner(System.in);

            // Do not forget, '\\' is the escape sequence for a backslash
            writer = new FileWriter("/Users/Admin/Desktop/test/fichier.txt");

            // We read line by line, a line ends with a newline character
            // (in Java a '\n') and then we write it into the file
            while (true) {

                inputText = reader.nextLine();

                // If you type 'EXIT', the application quits
                if (inputText.equals("EXIT")) {
                    break;
                }

                writer.write(inputText);

                // Add the newline character, because it is cut off by
                // the reader, when reading a whole line
                writer.write("\n");

            }

        } catch (IOException e) {

            // This exception may occur while reading or writing a line
            System.out.println("A fatal exception occurred!");

        } finally {

            // The finally branch is ALWAYS executed after the try
            // or potential catch block execution

            if (reader != null) {
                reader.close();
            }

            try {

                if (writer != null) {
                    writer.close();
                }

            } catch (IOException e) {

                // This second catch block is a clumsy notation we need in Java,
                // because the 'close()' call can itself throw an IOException.
                System.out.println("Closing was not successful.");

            }

        }

    }


}
	
//		try {
//			File file = new File("nomi.txt");
//			FileWriter fw = new FileWriter(file);
//			PrintWriter out = new PrintWriter("nomi2.txt");
//			InputStreamReader isr=new InputStreamReader(System.in);
//			BufferedReader keyboard= new BufferedReader(isr);
//			String input1, input2;
//
//			System.out.println("Inserire nome e cognome");
//
//		
//
//			input1 = keyboard.readLine();
//			input2 = keyboard.readLine();
//
//			System.out.println(input1);
//			System.out.println(input2);
//
//			out.println(input1);
//			out.println(input2);
//
//			fw.close();
//			out.close();
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//
//	}
