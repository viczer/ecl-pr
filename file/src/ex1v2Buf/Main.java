package ex1v2Buf;


import java.io.*;


public class Main {

	public static void main(String[] args) {
		try {

			BufferedReader in = new BufferedReader(new InputStreamReader(System.in));

			String lineFromInput = in.readLine();

			PrintWriter out = new PrintWriter(new FileWriter("fichier.txt"));

			out.println(lineFromInput);

			out.close();
		} catch (IOException e1) {
			System.out.println("Error during reading/writing");
		}

	}
}