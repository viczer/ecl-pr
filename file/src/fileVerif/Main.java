package fileVerif;

import java.io.*;

public class Main {

	public static void main(String[] args) {
		File my_file_dir = new File("/Users/Admin/Desktop/test/fichier.txt");
		if (my_file_dir.exists()) {
			System.out.println("The directory or file exist. \n");
			System.out.println(filesize_in_Bytes(my_file_dir));
			System.out.println(filesize_in_megaBytes(my_file_dir));
			System.out.println(filesize_in_kiloBytes(my_file_dir));
		} else {
			System.out.println("The directory or file not exist. \n");
		}

		System.out.println("----------------------------------------");
		File my_file_dir1 = new File("Users/Admin/Desktop/test/fichier.txt");
		if (my_file_dir1.isDirectory()) {
			System.out.println(my_file_dir1.getAbsolutePath() + " is a directory \n");
		} else {
			System.out.println(my_file_dir1.getAbsolutePath() + " is a not directory \n");
		}
		if (my_file_dir1.isFile()) {
			System.out.println(my_file_dir1.getAbsolutePath() + " is a file \n");
		} else {
			System.out.println(my_file_dir1.getAbsolutePath() + " is a not file \n");
		}
	}

	public static String filesize_in_megaBytes(File file) {
		return (double) file.length() / (1024 * 1024) + " mb";
	}

	public static String filesize_in_kiloBytes(File file) {
		return (double) file.length() / 1024 + " mb";
	}

	public static String filesize_in_Bytes(File file) {
		return (double) file.length() + " bytes";
	}
}
