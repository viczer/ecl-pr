package read;

import java.io.*;

public class Main {

	public static void main(String[] args) throws IOException {
		try {
			File file = new File("fichier1.txt");
			FileReader fr = new FileReader(file);
			BufferedReader bufferedReader = new BufferedReader(fr);
			String string = bufferedReader.readLine();
			
			while (string != null) {
				System.out.println(string);
				string = bufferedReader.readLine();
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

	}

}
