package ma.projet2;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

public abstract class Personne {
	protected int id;
	protected String nom;
	protected String prenom;
	protected Date dateN;
	protected String mail;
	protected String telephone;
	protected double salaire;
	protected Sport sport;
	private static int count;

	public Personne(String nom, String prenom, Date dateN, String mail, String telephone, double salaire, Sport sport) {
		super();
		this.id = ++count;
		this.nom = nom;
		this.prenom = prenom;
		this.dateN = dateN;
		this.mail = mail;
		this.telephone = telephone;
		this.salaire = salaire;
		this.sport = sport;
	}

	


	public Personne() {
		super();
		// TODO Auto-generated constructor stub
	}




	@Override
	public String toString() {
		//DateFormat df = DateFormat.getDateInstance(DateFormat.LONG, Locale.FRANCE);
		SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		String date = df.format(dateN);
		return "Personne [id=" + id + ", nom=" + nom + ", prenom=" + prenom + ", dateNais=" + date + ", mail=" + mail
				+ ", telephone=" + telephone + ", salaire=" + salaire + "]";
	}

	public abstract double calculerSalaire();

}
