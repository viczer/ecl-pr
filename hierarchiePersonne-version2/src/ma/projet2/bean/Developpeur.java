package ma.projet2.bean;

import java.util.Date;

import ma.projet2.Personne;
import ma.projet2.Sport;

public class Developpeur extends Personne {
	public Developpeur(String nom, String prenom, Date dateN, String mail, String telephone, double salaire,
			Sport sport) {
		super(nom, prenom, dateN, mail, telephone, salaire, sport);
		// TODO Auto-generated constructor stub
	}

	private String specialite;
	 


	@Override
	public double calculerSalaire() {
		return 1.2 * this.salaire;
	}
 
	public String affiche() {
		return "Le salaire du Developpeur " + this.nom + " " + this.prenom+ " est : " + this.calculerSalaire() + " sa specialite : "
				+ this.specialite + "   "+ "         dateN :" + dateN + "      SPORT : " + sport;
	}
 
}

