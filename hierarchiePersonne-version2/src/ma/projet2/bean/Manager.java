package ma.projet2.bean;

import java.util.Date;

import ma.projet2.Personne;
import ma.projet2.Sport;

public class Manager extends Personne {
	private String service;
 


	public Manager(String nom, String prenom, Date dateN, String mail, String telephone, double salaire, Sport sport,
			String service) {
		super(nom, prenom, dateN, mail, telephone, salaire, sport);
		this.service = service;
	}

	@Override
	public double calculerSalaire() {
		// TODO Auto-generated method stub
		return 1.35 * this.salaire;
	}
 
	public void affiche() {
		System.out.println("Le salaire du Manager " + this.nom + " " + this.prenom + " est " + this.calculerSalaire()
				+ " son service est " + this.service + "         dateN :" + dateN + "      SPORT : " + sport);
	}
 
}
