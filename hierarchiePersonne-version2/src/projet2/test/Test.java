package projet2.test;
import ma.projet2.Sport;
import ma.projet2.bean.*; 
import java.text.DateFormat;
import java.util.*;


	public class Test {
		 
		public static void main(String[] args) {
			Manager managers[] = new Manager[2];
			managers[0] = new Manager("Pippo", "Pi", new Date("1980/05/05"),"pippo.pi@gmail.com", "00000000",3500, Sport.HANDBALL,"Informatique");
			managers[1] = new Manager("Pluto", "Plu", new Date("1980/05/05"), "pluto.plu@gmail.com", "11111111", 7000,Sport.HANDBALL, "Administration");
			for (Manager m : managers)
				m.affiche();
	 
			Developpeur devs[] = new Developpeur[2];
			devs[0] = new Developpeur("Pippo", "Pi", new Date("1980/05/05"),"pippo.pi@gmail.com", "00000000",3500, Sport.BASKETBALL);
			devs[1] = new Developpeur("Pippo", "Pi", new Date("1980/05/05"),"pippo.pi@gmail.com", "00000000",3500, Sport.VOLLEBALL);
			for (Developpeur dev : devs) {
				System.out.println(dev.affiche());
				
			
			}
	 
		}
	}

