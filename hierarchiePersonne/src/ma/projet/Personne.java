package ma.projet;

public abstract class Personne {
	private int id;
	private String nom;
	private String prenom;
	private String mail;
	private String telephon;
	protected double salaire;
	
	public static int count;

	public Personne( String nom, String prenom, String mail, String telephon, double salaire) {
		super();
		this.id = ++count;
		this.nom = nom;
		this.prenom = prenom;
		this.mail = mail;
		this.telephon = telephon;
		this.salaire = salaire;
	}
	
	public abstract double calculerSalaire();

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getTelephon() {
		return telephon;
	}

	public void setTelephon(String telephon) {
		this.telephon = telephon;
	}

	public double getSalaire() {
		return salaire;
	}

	public void setSalaire(double salaire) {
		this.salaire = salaire;
	}

	@Override
	public String toString() {
		return "Personne id=" + id + ", nom=" + nom + ", prenom=" + prenom + ", mail=" + mail + ", telephon="
				+ telephon + ", salaire=" + salaire + "";
	}
	
	
}
