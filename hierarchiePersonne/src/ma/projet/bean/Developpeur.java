package ma.projet.bean;

import ma.projet.Personne;

public class Developpeur extends Personne {
	private String specialite;

	public Developpeur(String nom, String prenom, String mail, String telephon, double salaire, String specialite) {
		super(nom, prenom, mail, telephon, salaire);
		this.specialite = specialite;
	}
	

	public String getSpecialite() {
		return specialite;
	}


	public void setSpecialite(String specialite) {
		this.specialite = specialite;
	}


	@Override
	public double calculerSalaire() {
			return 1.2 * this.salaire;
	}

	@Override
	public String toString() {
		return "\n" + " Le salaire du "  + "developpeur " + getNom() + " " + getPrenom() + " est : " + this.calculerSalaire() + " euros" + ", son service : " + getSpecialite();
	}
	
	

}
