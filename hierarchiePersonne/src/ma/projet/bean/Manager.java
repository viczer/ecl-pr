package ma.projet.bean;

import ma.projet.Personne;

public class Manager extends Personne {
	private String service;

	public Manager(String nom, String prenom, String mail, String telephon, double salaire, String service) {
		super(nom, prenom, mail, telephon, salaire);
		this.service = service;
	}
	
	

	public String getService() {
		return service;
	}



	public void setService(String service) {
		this.service = service;
	}



	@Override
	public String toString() {
		return "\n" + " Le salaire du "  + "Manager " + getNom() + " " + getPrenom() + " est : " + this.calculerSalaire() + " euros" + ", son service : " + getService() ;
	}

	@Override
	public double calculerSalaire() {
		return 1.35 * this.salaire;
	}
	
	

}
