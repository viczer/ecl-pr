package project.test;
import java.util.*;

import ma.projet.bean.*;
public class Application {

	public static void main(String[] args) {
		
		System.out.print(" ");
		List<Manager> manager = new ArrayList<Manager>();
		manager.add(new Manager("Baton", "Jean", "baton.iean@gmail.com", null, 3500, "Informatique"));
		manager.add(new Manager("Baton2", "Jean2", "baton2.iean@gmail.com", null, 3900, "Informatique"));
		
		System.out.print(manager);
		System.out.print(" ");
		System.out.print(" \n");
		List<Developpeur> developpeur = new ArrayList<Developpeur>();
		developpeur.add(new Developpeur("Magoo", "Mister", "magoo.mister@gmail.com", null, 2900, "PHP"));
		developpeur.add(new Developpeur("Magoo2", "Mister2", "magoo2.mister@gmail.com", null, 2700, "PHP"));
		
		System.out.print(developpeur);
		System.out.print(" ");
		System.out.print(" ");

		
		
	}

	@Override
	public String toString() {
		return "Application ";
	}

}
