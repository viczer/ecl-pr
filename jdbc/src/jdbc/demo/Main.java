package jdbc.demo;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Main {

	public static void main(String[] args) throws ClassNotFoundException {
		String url = "jdbc:mysql://localhost:3306/jdbc?zeroDateTimeBehavior=CONVERT_TO_NULL&serverTimezone=UTC";
		String user = "root";
		String password = "root";
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			Connection connexion = DriverManager.getConnection(url, user, password);
			Statement statement = connexion.createStatement();
			String request = "SELECT * FROM Personne";
			ResultSet result = statement.executeQuery(request);//pour les requetes de lecture : select*********
																//ecuteUpdate() : inserty, update, delete , create
			
			//Recuperation des donnes
			while (result.next()) {
				int idPersonne = result.getInt("num");
				String nom = result.getString("nom");
				String prenom = result.getString("prenom");
				System.out.println(nom + " " + prenom + "  |   id personne : "  + idPersonne);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

}
