package jdbc.demo2;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Main {

	public static void main(String[] args) throws ClassNotFoundException {
		String url = "jdbc:mysql://localhost:3306/jdbc?zeroDateTimeBehavior=CONVERT_TO_NULL&serverTimezone=UTC";
		String user = "root";
		String password = "root";
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			Connection connexion = DriverManager.getConnection(url, user, password);
			Statement statement = connexion.createStatement();
			String request = "INSERT INTO personne (nom, prenom) VALUES ('Wick', 'John');";
			int nbr = statement.executeUpdate(request);// pour les requetes de lecture : select*********
														// ecuteUpdate() : inserty, update, delete , create

			if (0 != nbr)
				System.out.println("Insertion reussie");
			statement.executeUpdate(request, Statement.RETURN_GENERATED_KEYS);
			ResultSet resultat = statement.getGeneratedKeys();
			if (resultat.next()) {
				System.out.println("Le numero genere pour cette personne : " + resultat.getInt(1));
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

}