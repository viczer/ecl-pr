package jdbc.demo4_SET;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class Main {

	public static void main(String[] args) throws ClassNotFoundException {
		String url = "jdbc:mysql://localhost:3306/jdbc?zeroDateTimeBehavior=CONVERT_TO_NULL&serverTimezone=UTC";
		String user = "root";
		String password = "root";
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			Connection conn = DriverManager.getConnection(url, user, password);

			Statement state = conn.createStatement();

			String query = "UPDATE personne SET nom = 'dae', prenom = 'Jean' WHERE num = 1";

			int rowsInserted = state.executeUpdate(query);

			if (rowsInserted > 0) {
				System.out.println("The class was updated successfully!");
			}
			state.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

}