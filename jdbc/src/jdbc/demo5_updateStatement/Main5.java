package jdbc.demo5_updateStatement;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Main5 {

	public static void main(String[] args) throws ClassNotFoundException {
		String url = "jdbc:mysql://localhost:3306/jdbc?zeroDateTimeBehavior=CONVERT_TO_NULL&serverTimezone=UTC";
		String user = "root";
		String password = "root";
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			Connection c = DriverManager.getConnection(url, user, password);
			Statement state = c.createStatement();
			String request = "INSERT INTO personne (nom, prenom) VALUES (?, ?);";

			PreparedStatement ps = c.prepareStatement("update personne set nom = ?, prenom = ? where num = ? ");

			ps.setString(1, "Wick");
			ps.setString(2, "John");
			ps.setInt(3, 3);
			ps.executeUpdate();

			System.out.println("User with id " + " was updated in DB with following details: ");

		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

}