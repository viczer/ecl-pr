package jdbc.demo6_Delete;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

public class Main6_Delete {

	public static void main(String[] args) throws ClassNotFoundException {
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			String url = "jdbc:mysql://localhost:3306/jdbc?zeroDateTimeBehavior=CONVERT_TO_NULL&serverTimezone=UTC";
			String user = "root";
			String password = "root";
			Connection connexion = DriverManager.getConnection(url, user, password);
			Statement statement = connexion.createStatement();
			PreparedStatement ps = connexion.prepareStatement("delete from personne where num = 9");
			int rows = ps.executeUpdate();
			
			System.out.println("All the class was deleted successfully!");
//			if (rows == 0) {
//				System.out.println("All the class was deleted successfully!");
//			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}