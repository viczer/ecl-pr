package transaction;

import java.sql.*;

public class Main_TransDeleted {

	public static void main(String[] args) {
		String url = "jdbc:mysql://localhost:3306/jdbc?zeroDateTimeBehavior=CONVERT_TO_NULL&serverTimezone=UTC";
		String user = "root";
		String passwd = "root";
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			Connection conn = DriverManager.getConnection(url, user, passwd);
			/***************************/
			conn.setAutoCommit(false);
			/***************************/
			PreparedStatement ps = conn.prepareStatement("delete from personne where num = 14");
			int rowsDeleted = ps.executeUpdate();
			if(rowsDeleted>0) {
				System.out.println("All the class was deleted successfully!");
			}

			

			ResultSet result = ps.executeQuery("SELECT * FROM Personne");
			/*******************/
			result.first();
			/*********************/
			while (result.next()) {
				System.out.println(result.getString("num") + "Nom : " + result.getString("nom") + " - Prenom : "
						+ result.getString("prenom"));
			}
			/*******************************/
			conn.commit();
			/******************************/
			result.close();
			ps.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
