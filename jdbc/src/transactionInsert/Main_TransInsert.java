package transactionInsert;

import java.sql.*;

public class Main_TransInsert {

	public static void main(String[] args) {
		String url = "jdbc:mysql://localhost:3306/jdbc?zeroDateTimeBehavior=CONVERT_TO_NULL&serverTimezone=UTC";
		String user = "root";
		String passwd = "root";
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			Connection conn = DriverManager.getConnection(url, user, passwd);
			conn.setAutoCommit(false);
			String sql = "INSERT INTO Personne (nom,prenom) VALUES (?,?)";
			PreparedStatement state = conn.prepareStatement(sql);
			state.setString(1, "ZZZZZZZZZZZZZZZZZZZ");
			state.setString(2, "ZZZZZZZZZZZZZZZZZZZ");
			int rowsInserted = state.executeUpdate();
			if (rowsInserted > 0) {
				System.out.println("A new class was inserted successfully!");
			}
			ResultSet result = state.executeQuery("SELECT * FROM Personne");
			result.first();
			while (result.next()) {
				System.out
						.println("Nom : " + result.getString("nom") + " - Prenom : " + result.getString("prenom"));
			}
			conn.commit();
			result.close();
			state.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}