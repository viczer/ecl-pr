package jdbc_web.SELECT;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Main_Select {

	public static void main(String[] args) throws ClassNotFoundException {
		String url = "jdbc:mysql://localhost:3306/db_web?zeroDateTimeBehavior=CONVERT_TO_NULL&serverTimezone=UTC";
		String user = "root";
		String password = "root";
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			Connection connexion = DriverManager.getConnection(url, user, password);
			Statement statement = connexion.createStatement();
			String request = "SELECT * FROM user";
			ResultSet result = statement.executeQuery(request);//pour les requetes de lecture : select*********
																//ecuteUpdate() : inserty, update, delete , create
			
			//Recuperation des donnes
			while (result.next()) {
				int id= result.getInt("id");
				String nom = result.getString("nom");
				String prenom = result.getString("prenom");
				String sexe = result.getString("sexe");
				String rue = result.getString("rue");
				String codePostal = result.getString("codePostal");
				String ville = result.getString("ville");
				System.out.println(nom + " " + prenom + "  |   id personne : "  + id +"    |   sexe : " + sexe + "    |   rue : " + rue + "    |   code postal : " + codePostal + "    |   ville : " + ville  );
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

}
