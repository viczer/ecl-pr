package jdbc_web_INSERT;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Main_INSERT {

	public static void main(String[] args) throws ClassNotFoundException {
		String url = "jdbc:mysql://localhost:3306/db_web?zeroDateTimeBehavior=CONVERT_TO_NULL&serverTimezone=UTC";
		String user = "root";
		String password = "root";
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			Connection connexion = DriverManager.getConnection(url, user, password);
			Statement statement = connexion.createStatement();
			String request = "INSERT INTO user (nom, prenom, sexe, rue, codePostal, ville) VALUES (?, ?, ?, ?,?, ?);";
			PreparedStatement ps = connexion.prepareStatement(request, PreparedStatement.RETURN_GENERATED_KEYS);
			ps.setString(1, "Wick");
			ps.setString(2, "John");
			ps.setString(3, "M");
			ps.setString(4, "belvue");
			ps.setString(5, "05600");
			ps.setString(6, "Nice");
			
			ps.executeUpdate();
			ResultSet resultat = ps.getGeneratedKeys();
			if (resultat.next()) {
				System.out.println("Le numero genere pour cette personne : " + resultat.getInt(1));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

}