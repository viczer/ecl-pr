package org.eclipse.test;

import static org.junit.jupiter.api.Assertions.*;

import org.eclipse.main.Calcul;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class CalculTest {

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		System.out.println("------------------------");
        System.out.println("Avant Test");
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
		System.out.println("Apr�s Test");
        System.out.println("------------------------");
	}

	@BeforeEach
	void setUp() throws Exception {
		System.out.println("------------------------");
       
	}

	@AfterEach
	void tearDown() throws Exception {
		System.out.println("************************");
     
	}
	

	@Test
	void testSomme() {
		System.out.println("x+y test");
		Calcul calcul = new Calcul();
		if (calcul.somme(2, 3) != 5)
			fail("faux pour deux entiers positifs");
		if (calcul.somme(-2, -3) != -5)
			fail("faux pour deux entiers negatifs");
		if (calcul.somme(-2, 3) != 1)
			fail("faux pour deux entiers de signe different");
		if (calcul.somme(0, 3) != 3)
			fail("faux pour x null");
		if (calcul.somme(2, 0) != 2)
			fail("faux pour y null");
		if (calcul.somme(0, 0) != 0)
			fail("faux pour deux entiers null");

	}

	@Test
	void testDivision() {
		System.out.println("x/y Test");
		Calcul calcul = new Calcul();
		assertFalse(calcul.division(6, 3) == 0, "2 entiers positifs");
		assertEquals(2, calcul.division(-6, -3), "2 entiers negatifs");
		assertNotNull(calcul.division(-6, 3), "2 entiers de signe different");
		assertTrue(calcul.division(0, 3) == 0, "entier x nul");
		Throwable e = null;
		try {
			calcul.division(2, 0);
		} catch (Throwable ex) {
			e = ex;
		}
		assertTrue(e instanceof ArithmeticException);
		e = null;
		try {
			calcul.division(0, 0);
		} catch (Throwable ex) {
			e = ex;
		}
		assertTrue(e instanceof ArithmeticException);

	}
	@Test
	void testMultiplication() {
		System.out.println("x*y Test");
		Calcul calcul = new Calcul();
		assertFalse(calcul.multiplication(6, 3) == 0, "2 entiers positifs");
		assertEquals(18, calcul.multiplication(-6, -3), "2 entiers negatifs");
		assertNotNull(calcul.multiplication(-6, 3), "2 entiers de signe different");
		assertTrue(calcul.multiplication(0, 3) == 0, "entier x nul");
		Throwable e = null;
		try {
			calcul.multiplication(2, 0);
		} catch (Throwable ex) {
			e = ex;
		}
		assertTrue(e instanceof ArithmeticException);
		e = null;
		try {
			calcul.multiplication(0, 0);
		} catch (Throwable ex) {
			e = ex;
		}
		assertTrue(e instanceof ArithmeticException);

	}
	
	@Test
	void testSottration() {
		System.out.println("x-y Test");
		Calcul calcul = new Calcul();
		assertEquals(-1,calcul.sottration(2, 3), "2 entiers positifs");
		assertEquals(0,calcul.sottration(2, 2), "2 entiers x=y");
		assertEquals(1, calcul.sottration(-2, -3), "2 entiers negatifs");
		assertNotNull(calcul.sottration(-2, 3), "2 entiers de signe different");
		assertTrue(calcul.sottration(0, 3) == -3, "entier x nul");
		
		
//		if (calcul.sottration(2, 3) != -1)
//			fail("faux pour deux entiers positifs");
//		if (calcul.sottration(-2, -3) != 1)
//			fail("faux pour deux entiers negatifs");
//		if (calcul.sottration(-2, 3) != -5)
//			fail("faux pour deux entiers de signe different");
//		if (calcul.sottration(0, 3) != -3)
//			fail("faux pour x null");
//		if (calcul.sottration(2, 0) != 2)
//			fail("faux pour y null");
//		if (calcul.sottration(0, 0) != 0)
//			fail("faux pour deux entiers null");

	}

}
