package app;

import java.util.Scanner;

import models.BorneEntree;
import models.BorneSortie;
import models.FullDbException;
import models.ITicketDB;
import models.IdNotFoundException;
import models.TicketDBSet;


public class Main {
	public static void main(String[] args) throws IdNotFoundException  {

		Scanner s = new Scanner(System.in);

		// creation des bornes
		BorneEntree be = new BorneEntree();
		BorneSortie bs = new BorneSortie();
		//creation de la base de donnee
		ITicketDB db = new TicketDBSet();
		be.setDb(db);
		bs.setDb(db);

		// debut boucle

		int choix;
		do {
			//affichage de la DB
			System.out.println(db);
			// affichage du menu 

			System.out.println("Votre choix:");
			System.out.println("1. entrer");
			System.out.println("2. sortir");
			System.out.println("3. quitter");

			// lecture du choix de l utilisateur
			try {
				choix= Integer.parseInt(s.nextLine());
			} catch(NumberFormatException e ) {	
				//	   System.out.println("exception NumberFormatException captur�e ("+ e.getMessage() +")");
				//		e.printStackTrace();
				choix = -1;
			}
			// execution de l action choisie

			switch (choix) {
			case 1:
				System.out.println("on entre dans le parking");
				try {
					be.appuyerBouton();
				} catch (FullDbException e) {
					System.out.println("parking plein");
				}	 catch (IdNotFoundException e) {
					System.out.println("erreur innatendue. le systeme nest plus fiable");
					throw e;
				}
				break;
			case 2:
				System.out.println("On sort du parking");
				bs.attendreSortie();
				break;
			case 3:
				System.out.println("aurevoir");
				break;
			default: 
				System.out.println("Choix invalide");
			}
			// fin boucle 


		}
		while (choix != 3); 
		// fermeture du scanner

		s.close();
	}
}













