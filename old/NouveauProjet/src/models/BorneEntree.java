package models;

public class BorneEntree extends Borne {

	private ImprimanteTicket imprimante = new ImprimanteTicket();

		
		public void appuyerBouton() throws FullDbException, IdNotFoundException {
			System.out.println("appuyer bouton()");

			//creation ticket
			Ticket t = new Ticket();
			// ajout a la base de donnee
			db.save(t);
			//impression d
			//impression du ticket
			imprimante.imprimerTicket(t);
			//attendre la saisie du ticket
			if(imprimante.attendreSaisieTicket(5)) {
				// si le ticket a ete saisie
				barriere.ouvrir();
				if (capteur.attendrePassage(5)) {
					//si le vehicule est passe
					//on ferme la barriere
					barriere.fermer();
				}else {
					// sinon//on supprime le ticket
					db.delete(t.getId());
					barriere.fermer();
				}
				}else {
					// sinon//on supprime le ticket
					db.delete(t.getId());
				
				}
			}
		
		
	}