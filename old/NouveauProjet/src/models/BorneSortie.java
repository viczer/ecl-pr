package models;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

public class BorneSortie extends Borne {
	private ModuleCb moduleCb = new ModuleCb();
	
	private LecteurTicket lecteurTicket = new LecteurTicket();
	public void attendreSortie() throws IdNotFoundException {
		System.out.println("attendre sortie()");
		
		Ticket t;
		
		try {
			t = lecteurTicket.lireTicket(db);
		} catch (IdNotFoundException e) {
			System.out.println("la carte inser�e n est pas valide");
			return;
		}
		// calcul montant
		double m = calculMontant(t);
	// demande paiement
		moduleCb.demanderPaiement(m);
		//attendre paiement
		if (moduleCb.attendrePaiement(5)) {
			// ouverture barriere
			barriere.ouvrir();
			// attendre passage
			if (capteur.attendrePassage(5)) {
				//le vehicule est passe
				//fermeture barriere
				barriere.fermer();
				//actualisation ticket
				t.payer(m);
				t.sortir();
				
			} else {
				// le vehicule nest pas passer
				// fermeture barriere
				barriere.fermer();
				//actualisation ticket
				t.payer(m);
			
		}
			// paiement echouer
		}else {
			
		}
	}

	public double calculMontant(Ticket t) {
		System.out.println("calcul du montant("+t+")");
		LocalDateTime now = LocalDateTime.now();
		long p = t.getDateEntree().until(now, ChronoUnit.SECONDS);
				
		double m = p;
		return m;
}

	public ModuleCb getModuleCb() {
		return moduleCb;
	}

	public void setModuleCb(ModuleCb moduleCb) {
		this.moduleCb = moduleCb;
	}

	public LecteurTicket getLecteurTicket() {
		return lecteurTicket;
	}

	public void setLecteurTicket(LecteurTicket lecteurTicket) {
		this.lecteurTicket = lecteurTicket;
	}


		

	}
