package models;

public class IdNotFoundException extends Exception {
	private int id;
	
	public IdNotFoundException(int id, String message ) {
		super(message);
		this.id = id;
		
	}
	public IdNotFoundException(int id) {
		super("Id" + id + " not found in DB");
		this.id = id;
		}
	public int getId() {
		return id;
	}
}