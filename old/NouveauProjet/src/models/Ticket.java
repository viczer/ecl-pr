package models;

import java.time.LocalDate;
import java.time.LocalDateTime;
public class Ticket {
	
	private int id;

	@Override
	public String toString() {
		return "Ticket [id=" + id + ", dateEntree=" + dateEntree + ", dateSortie=" + dateSortie + ", paye=" + paye
				+ "]";
	}
	private LocalDateTime dateEntree;
	private LocalDateTime dateSortie;
	private double paye;
	
	private static int nextId = 0;
	
	public Ticket() {
		this.id= nextId++;
		this.dateEntree= LocalDateTime.now();
		this.dateSortie = null;
		this.paye= 0;
	}
	public void payer(double m) {
		paye +=m;
	}
	public void sortir() {

		this.setDateSortie(LocalDateTime.now());
	
}

	public void setDateEntree(LocalDateTime dateEntree) {
		this.dateEntree = dateEntree;
	}
	public double getPaye() {
		return paye;
	}
	public void setPaye(double paye) {
		this.paye = paye;
	}
	public LocalDateTime getDateSortie() {
		return dateSortie;
	}
	public void setDateSortie(LocalDateTime dateSortie) {
		this.dateSortie = dateSortie;
	}
	public int getId() {
		return id;
	}
	public LocalDateTime getDateEntree() {
		return dateEntree;
	}
		}

