package models;
import models.Ticket;
import java.util.ArrayList;
import java.util.List;

public class TicketDBList implements ITicketDB {

	private List<Ticket> tickets = new ArrayList<Ticket>();



	@Override
	public void save(Ticket t) throws FullDbException {
		tickets.add(t);
	}

	public Ticket find(int id) throws IdNotFoundException {
		for (Ticket t : tickets) {
			if (t.getId() == id)
				return t;
		}
		throw new IdNotFoundException(id);
	}

	@Override
	public void delete(int id) throws IdNotFoundException {
		for (int i=0; i<tickets.size(); ++i) {
			if (tickets.get(i).getId() == id) {
				tickets.remove(i);	
				return ;
			}
		}
		throw new IdNotFoundException(id);
	}

	@Override
	public String toString() {
		String ret = "";
		for (Ticket t: tickets)
			ret = ret + "\t" + t + "\n";
		return ret;
	}
}

