package models;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class TicketDBSet implements ITicketDB {

	private Set<Ticket> tickets = new HashSet<Ticket>();



	@Override
	public void save(Ticket t) throws FullDbException {
		tickets.add(t);
	}

	public Ticket find(int id) throws IdNotFoundException {
		for (Ticket t : tickets) {
			if (t.getId() == id)
				return t;
		}
		throw new IdNotFoundException(id);
	}

	@Override
	public void delete(int id) throws IdNotFoundException {
		Iterator<Ticket> it = tickets.iterator();
		while (it.hasNext()) {
			Ticket t = it.next();
					if (t.getId() == id) {
						tickets.remove(t);
						return ;
		
//		for (int i=0; i<tickets.size(); ++i) {
	//		if (tickets.get(i).getId() == id) {
	//			tickets.remove(i);	
				
	//			
				
			}
		}
		throw new IdNotFoundException(id);
	}

	@Override
	public String toString() {
		String ret = "";
		for (Ticket t: tickets)
			ret = ret + "\t" + t + "\n";
		return ret;
	}



	}

