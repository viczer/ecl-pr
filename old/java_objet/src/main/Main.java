package main;

import model.Adresse;
import model.Enseignant;
import model.Etudiant;
import model.Person;

public class Main {
   
	public static void main(String[] args) {
		Person p1, p2;
	    System.out.println(Person.getNbPersonnes());
    	p1 = new Person();
	    System.out.println(Person.getNbPersonnes());
		p1.setNum(1);
    	p1.setNom("wick");
    	p1.setPrenom("john");
    	p1.setAdresse(new Adresse("bonne mere", "13001","Marseille"));
    	System.out.println("nbPersonnes = " + Person.getNbPersonnes());
    	p2 = new Person(2, "wick", "john", new Adresse("Cannebiere", "13001", "Marseille"));
	    System.out.println(Person.getNbPersonnes());
		System.out.println(p1);
		System.out.println(p2);

		
		Etudiant etudiant = new Etudiant();
		etudiant.setNum(3);
    	etudiant.setNom("wick");
    	etudiant.setPrenom("john");
    	etudiant.setAdresse(new Adresse("A", "B", "C"));
    	etudiant.setNiveau("pas terrible");
    	System.out.println(etudiant);
    	
    	Etudiant etudiant2 = new Etudiant(4, "wick", "john", new Adresse("A", "B,", "C"), "pas mieux");
    	Enseignant enseignant = new Enseignant(4, "wick", "john", new Adresse("A","B", "C"), 1000000);
    	
    	Person [] annuaire = {etudiant2, enseignant};
    	for (int i=0; i<annuaire.length; ++i) {
    		System.out.println("\t" + annuaire[i]);
    	}
    	}
    	
    	
		
		
		
	}
	
