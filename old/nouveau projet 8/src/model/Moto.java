package model;

public class Moto extends VehiculeRoulant {

	
		public Moto(int lon, int lar, String nom) {
			super();
			this.longueur = lon;
			this.largeur = lar;
			this.nom = nom;
		}
		
		
		@Override
		public String toString() {
			return "moto [lon=" + longueur + ", lar=" + largeur + ", nom=" + nom + "]";
		}


		public Moto() {
			super();
			// TODO Auto-generated constructor stub
		}

		
		public void mettreLesGazs(int distance) {
			System.out.println(this + " ");
			for (int i=0; i<distance; ++i)
				System.out.print("-");
				System.out.println();
		}
		
		
		
		
	public void tournerLeGuidon(int degres) {
		System.out.println(this + " tourne de " + degres + " degres ");
			}


	@Override
	public void avancer(int distance) {
		// TODO Auto-generated method stub
		mettreLesGazs(distance);
	}


	@Override
	public void tourner(int degres) {
		// TODO Auto-generated method stub
		tournerLeGuidon(degres);
	}


	@Override
	public char getLogo() {
		// TODO Auto-generated method stub
		return ('8');
	}
		
}
