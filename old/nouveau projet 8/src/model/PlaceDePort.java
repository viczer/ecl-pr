package model;

public class PlaceDePort {

	 private int lonMax;
	 private int larMax;
	 private int pronfMax;
	 private Bateau vehicule;
	
	 public PlaceDePort(int lonMax, int larMax, int pronfMax, YIVehiculeNav vehicule) {
		super();
		this.lonMax = lonMax;
		this.larMax = larMax;
		this.pronfMax = pronfMax;
		this.vehicule = vehicule;
		
	}
	public PlaceDePort() {
		super();
		// TODO Auto-generated constructor stub
	}
	public YIVehiculeNav getVehicule() {
		return vehicule;
	}
	public void setVehicule(Bateau b1) {
		this.vehicule = b1;
	}
	 
	public String toString() {
		if (estLibre())
			return" ";
		else
        return ""+vehicule.getLogo();	 
	}
		public boolean estLibre() {
			return (vehicule == null);
		}
	 
		public boolean peutTenir1(YIVehiculeNav v) {
			if ((lonMax >= v.getLongueur()) &&
				(larMax >= v.getLargeur()) &&
				(pronfMax >= v.getPronfondeur()))
				return true;
			else
				return false;
			
		}
		public boolean peutTenir(VehiculeNav v) {
			// TODO Auto-generated method stub
			return false;
		}
	 
}

