package model;
public abstract class VehiculeNav {   
	//attributs    
	
	protected int longueur,largeur,pronfondeur;    
	public int getPronfondeur() {
		return pronfondeur;
	}
	public void setPronfondeur(int pronfondeur) {
		this.pronfondeur = pronfondeur;
	}
	protected String nom;           

	//getter / setters    
	
	public int getLongueur() {     
		return longueur;         }    
	public void setLongueur(int longueur) {       
		this.longueur = longueur;         }        
	public int getLargeur() {        
		return largeur;         }       
	public void setLargeur(int largeur) { 
		this.largeur = largeur;         }    
	public String getNom() {           
		return nom;         }       
	public void setNom(String nom) {      
		this.nom = nom;         }       
	
	//methode toString 
	
	
	@Override      
	public String toString() {       
		return "Voitures [nom=" + nom + "]";         }
	protected abstract void naviguer(int i);
	protected abstract void virerDeBord(int i);
	protected abstract char getLogo();   
	
	//methodes abstraites 
	
	
}
