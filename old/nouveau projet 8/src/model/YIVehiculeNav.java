package model;

public interface YIVehiculeNav {
	
	public abstract void virerDeBord(int distance);
	public abstract void naviguer(int degres);
	public default char getLogo() {
	return 'N';
}
	public abstract int getLongueur();
	public abstract int getLargeur();
	public abstract int getPronfondeur();
	
}
