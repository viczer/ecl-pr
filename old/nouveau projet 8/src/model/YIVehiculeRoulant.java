package model;

public interface YIVehiculeRoulant {

	public void avancer(int distance);
	public void tourner(int degre);
	
	public default char getLogo() {
		return 'X';
	}
	public int getLongueur();
	public int getLargeur();
	

}
