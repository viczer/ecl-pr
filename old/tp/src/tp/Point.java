package tp;

public class Point {
	private double abs;
	private double ord;
	public double getAbs() {
		return abs;
	}
	public void setAbs(double abs) {
		this.abs = abs;
	}
	public double getOrd() {
		return ord;
	}
	public void setOrd(double ord) {
		this.ord = ord;
	}
	public Point(double abs, double ord) {
		super();
		this.abs = abs;
		this.ord = ord;
	}
	
	public double calculerDistance(Point p) {
		return Math.sqrt(
				(p.abs - this.abs)*(p.abs - this.abs) +
				(p.ord - this.ord)*(p.ord - this.ord));
				
	}
	public Point calculerMilieu(Point p) {
		double x= (this.abs + p.abs)/2;
		double y= (this.ord + p.ord)/2;
		return new Point(x, y);
		
		
				
		
	}

}
