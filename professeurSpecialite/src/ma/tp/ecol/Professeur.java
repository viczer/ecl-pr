package ma.tp.ecol;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Professeur {
	private int id;
	private String nom;
	private String prenom;
	private Date dateNais;
	private Specialite specialite;
	
	private static int count;

	public Professeur(String nom, String prenom, Date dateNais, Specialite specialite) {
		super();
		this.id = ++count;
		this.nom = nom;
		this.prenom = prenom;
		this.dateNais = dateNais;
		this.specialite = specialite;
	}

	public Professeur() {
		super();
		
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public Date getDateNais() {
		return dateNais;
	}

	public void setDateNais(Date dateNais) {
		this.dateNais = dateNais;
	}

	public Specialite getSpecialite() {
		return specialite;
	}

	public void setSpecialite(Specialite specialite) {
		this.specialite = specialite;
	}

	@Override
	public String toString() {
		SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		String date = df.format(dateNais);
		return nom + " " + prenom + date;
	}
	
	
	
}
