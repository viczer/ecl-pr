package ma.tp.ecol;

public class Specialite {
	private int id;
	private String code;
	private String libelle;
	
	private static int count;

	public Specialite(String code, String libelle) {
		super();
		this.id = ++count;
		this.code = code;
		this.libelle = libelle;
	}

	public Specialite() {
		super();
		// TODO Auto-generated constructor stub
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	@Override
	public String toString() {
		return "Specialite [id=" + id + ", code=" + code + ", libelle=" + libelle + "]";
	}
	
	
	
}
