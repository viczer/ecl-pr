package ma.tp.ecol;

import java.util.Date;

public class Test {

	public static void main(String[] args) {
		Specialite[] specialites = new Specialite[5];
		// Cr�ation des sp�cialit�s
		specialites[0] = new Specialite("S1", "JAVA/JEE");
		specialites[1] = new Specialite("S2", ".net");
		specialites[2] = new Specialite("S3", "Gestion de projet");
		specialites[3] = new Specialite("S4", "CISCO");
		specialites[4] = new Specialite("S5", "PHP");

		// Cr�ation des professeurs
		Professeur professeurs[] = new Professeur[4];
		professeurs[0] = new Professeur("Safi", "salim", new Date(1980/05/05), specialites[0]);
		professeurs[1] = new Professeur("Rami", "amal", new Date(1980/05/05), specialites[3]);
		professeurs[2] = new Professeur("Rashidi", "Mohamed", new Date(1980/05/05), specialites[0]);
		professeurs[3] = new Professeur("Simon", "thomas", new Date(1980/05/05), specialites[3]);

		// Professeur par sp�cialit�
		System.out.println("Professeur par sp�cialit� : ");
		for (Specialite s : specialites) {
			System.out.println("\t" + s);
			int etat = 0;
			for (Professeur p : professeurs) {
				if (p.getSpecialite().getId() == s.getId()) {
					System.out.println("\t\t" + p);
					etat = 1;
				}
			}
			if (etat == 0) {
				System.out.println("\t\tAucun professeur dans cette sp�cialit�");
			}
		}

	}

}

