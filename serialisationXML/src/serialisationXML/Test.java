package serialisationXML;

public class Test {

	public static void main(String[] args) {
		try  {
			User user = new User("admin", "azerty");
			XMLToolsjava.encodeToFile(user, "user.xml");
			System.out.println(user);
			user = new User("newAdmin", "123456");
			System.out.println(user);
			user = (User) XMLToolsjava.decodeFromFile("user.xml");
			System.out.println(user);
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}

}

