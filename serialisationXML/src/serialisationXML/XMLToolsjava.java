package serialisationXML;

import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.*;

import java.io.*;

public class XMLToolsjava {

	public static void encodeToFile(Object object, String fileName)
			throws FileNotFoundException, IOException, IntrospectionException {

		XMLEncoder encoder = new XMLEncoder(new FileOutputStream(fileName));
		try {
			BeanInfo info = Introspector.getBeanInfo(User.class);
			PropertyDescriptor[] propertyDescriptors = info.getPropertyDescriptors();
			for (PropertyDescriptor descriptor : propertyDescriptors) {
				if (descriptor.getName().contentEquals("password")) {
					descriptor.setValue("transient", Boolean.TRUE);
				}
			}

			encoder.writeObject(object);
			encoder.flush();
		} finally {

			encoder.close();
		}
	}

	public static Object decodeFromFile(String fileName) throws FileNotFoundException, IOException {
		Object object = null;

		XMLDecoder decoder = new XMLDecoder(new FileInputStream(fileName));
		try {

			object = decoder.readObject();
		} finally {

			decoder.close();
		}

		return object;

	}
}