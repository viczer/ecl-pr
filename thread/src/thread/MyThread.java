package thread;
//	implements	Runnable  extends Thread 
class MyThread implements Runnable {
	private String message;

	public MyThread(String m) {
		message = m;
	}

	public void run() {
		for (int r = 0; r < 9; r++)
			System.out.println(message);
	}
}

class ProvaThread {
	public static void main(String[] args) {
		Thread t1, t2;
		MyThread r1, r2;
		r1 = new MyThread("primo thread");
		r2 = new MyThread("secondo thread");
		t1 = new Thread(r1);
		t2 = new Thread(r2);
		t1.start();
		t2.start();
	}
}