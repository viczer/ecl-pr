package numero;

import javax.swing.JOptionPane;
import java.util.Random;

public class Listing9_3 {
	public static void main(String[] args) {
		Random rnd = new Random(System.currentTimeMillis());
		int secret = 1 + rnd.nextInt(10);
		int userData = 0;
		String userInput;
		while (true) {
// Выводим окно запроса
			userInput = JOptionPane.showInputDialog("Guess a number from 1 to 10");
// проверка опасного участка кода
			try {
// Преобразуем строку в число в явном виде
				userData = Integer.parseInt(userInput);
// проверяем введенное число на совпадение с секретным
				if (userData == secret) {
					JOptionPane.showMessageDialog(null, "You guessed the number!");
					break;
				}
			}
// обработчик исключения
			catch (NumberFormatException e) {
// если пользователь нажал кнопку «Cancel»
				if (e.toString().contains("null")) {
// прерывание работы программы
					System.exit(0);
				}
// если пользователь ввел недопустимое значение
				System.out.println(e);
				JOptionPane.showMessageDialog(null, "Invalid value!", "Error", JOptionPane.ERROR_MESSAGE);
			}
		}
	}
}