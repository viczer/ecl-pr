package numero;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Q1 {
	public static void entryCorrect() {
		Scanner sc = new Scanner(System.in);
		System.out.println("Donnez un entier : ");
		int n = sc.nextInt();
		System.out.println("L'entier saisi est : " + n);
	}
 //Main mathod
	public static void main(String[] args) {
		while (true) {
			try {
				entryCorrect();
				break;
			} catch (InputMismatchException e) {
				System.out.println("Erreur de saisi");
			} finally {
				System.out.println("Fin");
			}
		}
	}
}
