package numero;
import java.util.InputMismatchException;
import java.util.Scanner;
 
public class Q2 {
 
	public static void entryCorrect() throws IllegalArgumentException {
		Scanner sc = new Scanner(System.in);
		System.out.println("Donnez un entier : ");
		int n = sc.nextInt();
		if (n < 10)
			throw new IllegalArgumentException("valeur < 10");
		System.out.println("L'entier saisi est : " + n);
	}
 
	public static void main(String[] args) {
		while (true) {
			try {
				entryCorrect();
				break;
			} catch (IllegalArgumentException e) {
				System.out.println(e.getMessage());
			} catch (InputMismatchException e) {
				System.out.println("Erreur de saisi");
			} 
		}
	}
 
}
