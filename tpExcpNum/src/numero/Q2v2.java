package numero;
import java.util.InputMismatchException;
import numero.NumberException;
import java.util.Scanner;

public class Q2v2 {
	public static void entryCorrect() throws NumberException {
		Scanner sc = new Scanner(System.in);
		System.out.println("Donnez un entier : ");
		int n = sc.nextInt();
		if (n < 10)
			throw new NumberException("valeur < 10");
		System.out.println("L'entier saisi est : " + n);
	}
 
	public static void main(String[] args) {
		while (true) {
			try {
				entryCorrect();
				break;
			} catch (NumberException e) {
				System.out.println(e.getMessage());
			} catch (InputMismatchException e) {
				System.out.println("Erreur de saisi");
			} 
		}
	}


}
