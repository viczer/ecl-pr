package personnel;

abstract class Commerce extends Employe {
	private double chiffreAffaire;

	public Commerce(String nom, String prenom, int age, String date, double chiffreAffaire) {
		super(nom, prenom, age, date);
		this.chiffreAffaire = chiffreAffaire;
	}

	public double getChiffreAffaire() {
		return chiffreAffaire;
	}

}
