package personnel;

public class Manutentionnaire extends Employe {
	private int heures;

	public Manutentionnaire(String nom, String prenom, int age, String date, int heures) {
		super(nom, prenom, age, date);
		this.heures = heures;
	}

	public double calculerSalaire() {
		return 65 * heures;
	}

	public String getPosition() {
		return "Le manutentionnaire ";
	}
}
