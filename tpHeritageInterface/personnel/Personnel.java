package personnel;

public class Personnel {

	private Employe[] pers;
	private int countEmploye;
	private int maxEmp = 200;

	public Personnel() {
		pers = new Employe[maxEmp];
		countEmploye = 0;
	}

	public void ajouterEmploye(Employe e) {
		++countEmploye;
		if (countEmploye <= maxEmp) {
			pers[countEmploye - 1] = e;
		} else {
			System.out.println("Pas plus de " + maxEmp + " employ�s");
		}
	}

	public void afficherSalaires() {
		for (int i = 0; i < countEmploye; i++) {
			System.out.println(pers[i].getNom() + " gagne " + pers[i].calculerSalaire() + " francs.");
		}
	}

	public double salaireMoyen() {
		double sum = 0.0;
		for (int i = 0; i < countEmploye; i++) {
			sum += pers[i].calculerSalaire();
		}
		return sum / countEmploye;
	}

}

