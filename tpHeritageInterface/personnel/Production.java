package personnel;

public class Production extends Employe {
	private int unites;

	public Production(String nom, String prenom, int age, String date, int unites) {
		super(nom, prenom, age, date);

		this.unites = unites;
	}

	public double calculerSalaire() {
		return 5 * unites;
	}

	public String getPosition() {
		return "Le production ";
	}

}









