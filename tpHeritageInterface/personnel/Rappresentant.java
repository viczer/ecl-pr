package personnel;

public class Rappresentant extends Commerce {
	private double percRappr = 0.2;
	private int bonusRappr = 800;

	public Rappresentant(String prenom, String nom, int age, String date, double chiffreAffaire) {
		super(prenom, nom, age, date, chiffreAffaire);
	}

	public double calculerSalaire() {
		return (percRappr * getChiffreAffaire()) + bonusRappr;
	}

	public String getPosition() {
		return "Le représentant ";
	}
}
