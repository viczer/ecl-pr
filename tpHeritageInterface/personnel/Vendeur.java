package personnel;

public class Vendeur extends Commerce {
	private double percVendeur = 0.2;
	private int bonusVendeur = 400;

	public Vendeur(String prenom, String nom, int age, String date, double chiffreAffaire) {
		super(prenom, nom, age, date, chiffreAffaire);
	}

	public double calculerSalaire() {
		return (percVendeur * getChiffreAffaire()) + bonusVendeur;
	}

	public String getPosition() {
		return "Le vendeur ";
	}

}