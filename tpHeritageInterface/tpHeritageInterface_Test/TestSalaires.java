package tpHeritageInterface_Test;

import personnel.*;

public class TestSalaires {

	public static void main(String[] args) {

		Personnel pers = new Personnel();
		pers.ajouterEmploye(new Rappresentant("Pippo", "Baudo", 48, "1991", 20000));
		pers.ajouterEmploye(new Vendeur("Pluto", "Pluto", 35, "2000", 30000));
		pers.ajouterEmploye(new Production("Rossi", "Roberto", 28, "2005", 1000));
		pers.ajouterEmploye(new ProdRisques("Zeruk", "Victoria", 25, "2011", 1000));
		pers.ajouterEmploye(new Manutentionnaire("Fabrice", "Fabr", 44, "2004", 45));
		pers.ajouterEmploye(new ManutRisques("Fort", "Albert", 30, "2001", 45));
		

		pers.afficherSalaires();
		System.out.println("Le salaire moyen dans l'entreprise est de " + pers.salaireMoyen() + " francs.");
	}

}
