package ma.projet.bean;

import java.text.DateFormat;
import java.util.*;


import ma.projet.inter.IPersonne;

public class Personne implements IPersonne{
	private int id;
	private String nom;
	private String prenom;
	private Date dateNais;
	private double salaire;
	private Profil profil;
	public static int count;
	
	
	public Personne(String nom, String prenom, Date dateNais, double salaire) {
		super();
		this.id = ++count;
		this.nom = nom;
		this.prenom = prenom;
		this.dateNais = dateNais;
		this.salaire = salaire;
		
	}
	

	@Override
	public void affiche() {
		DateFormat df = DateFormat.getDateInstance(DateFormat.LONG, Locale.FRANCE);
		String date  = df.format(dateNais);
		System.out.println("Je suis le "+this.profil.getLibelle() +" "+nom+" "+prenom+ " n� le "+date+ " mon salaire est "+calculerSalaire()); 
	}
	@Override
	public double calculerSalaire() {		
			if(this.profil.getCode().equals("A1")) return this.salaire = this.salaire + 0.2*this.salaire;
			if(this.profil.getCode().equals("2")) return this.salaire = this.salaire + 0.1*this.salaire;
			return this.salaire;
		}
		
	}
	


