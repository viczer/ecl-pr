package ma.projet.bean;

public class Profil {
	private int id;
	private String code;
	private String libelle;
	private static int count;

	public Profil(String code, String libelle) {
		super();

		this.id = ++count;

		this.code = code;
		this.libelle = libelle;

	}

	public String getCode() {
		return code;
	}

	public String getLibelle() {
		return libelle;
	}

}
