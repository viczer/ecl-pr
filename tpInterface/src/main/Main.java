package main;

import java.util.Date;

import ma.projet.bean.Personne;
import ma.projet.bean.Profil;

public class Main {	
	
	
			
	public static void main(String[] args) {
		Profil p1 = new Profil("A1", "Admin");
		Personne pers1 = new Personne("Fabrice", "Fabrice",new Date("1980/05/01") ,3000.0);
		System.out.println(pers1);
		pers1.affiche();
		System.out.println("Mon nouveau salaire : " + pers1.calculerSalaire());
		
//			Profil profiles[] = new Profil[2];
//		    profiles[0] = new Profil("1","Developpeur");
//			profiles[1] = new Profil("2","Administrativ");
//			Personne personnes[] = new Personne[2];
//			personnes[0] = new Personne("Fabrice", "Fabrice",new Date("1980/05/01") ,3000.0);
//			personnes[1] = new Personne("Victoria", "Victoria",new Date("1970/06/11"), 2200.00 );
//			for(Personne pers : personnes){pers.affiche();
//			}
	}

}
