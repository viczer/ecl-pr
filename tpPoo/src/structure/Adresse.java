package structure;

public class Adresse {

	public int codePostal;
	public String gouvernorat;
	public String ville;

	public Adresse(int codePostal, String gouvernorat, String ville) {
		super();
		this.codePostal = codePostal;
		this.gouvernorat = gouvernorat;
		this.ville = ville;
	}
	
	public Adresse() {
		super();
	}


	public void afficher() {
		System.out.println("Code postal: " + codePostal + " gouvernorat: " + gouvernorat + " Ville: " + ville);
	}

	public void modifier(int c) {

		codePostal = c;
	}

	public String toString() {
		return "Code postal: " + codePostal + " gouvernorat: " + gouvernorat + " Ville: " + ville;
	}

}


  	    