package structure;

import java.util.Arrays;

import personnel.Chercheur;

public class Bureau {
	public int code;
	public String nom;
	public Chercheur chercheur[];

	public Bureau(int code, String nom, Chercheur[] chercheur) {
		super();
		this.code = code;
		this.nom = nom;
		this.chercheur = chercheur;
	}

	public Bureau() {
		super();
		// TODO Auto-generated constructor stub
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public Chercheur[] getChercheur() {
		return chercheur;
	}

	public void setChercheur(Chercheur[] chercheur) {
		this.chercheur = chercheur;
	}

	@Override
	public String toString() {
		return "Bureau [code=" + code + ", nom=" + nom + ", chercheur=" + Arrays.toString(chercheur) + ", getCode()="
				+ getCode() + ", getNom()=" + getNom() + ", getChercheur()=" + Arrays.toString(getChercheur())
				+ ", getClass()=" + getClass() + ", hashCode()=" + hashCode() + ", toString()=" + super.toString()
				+ "]";
	}

	public void afficher() {
		// TODO Auto-generated method stub

	}

}
