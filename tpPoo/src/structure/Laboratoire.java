package structure;

public class Laboratoire {
	public String nom;
	public String specialite;
	public Adresse adresse;
	public Bureau bureau[];

	public Laboratoire(String nom, String specialite, Adresse adresse, Bureau[] bureau) {
		super();
		this.nom = nom;
		this.specialite = specialite;
		this.adresse = adresse;
		this.bureau = new Bureau[50];
		this.bureau = bureau;
	}

	public Laboratoire() {
		super();
	}

	public void afficher() {

		System.out.println(" Nom: " + nom + " specialite" + specialite);
		this.adresse.afficher();
		int i;
		int n = this.bureau.length;

		for (i = 0; i < n; i++) {
			this.bureau[i].afficher();
		}

	}

	@Override
	public String toString() {

		String result = "*** " + nom + " ******";
		result += specialite;
		result += "adresse: " + adresse;

		for (Bureau BB : bureau) {
			if (BB != null)
				result += BB;
		}

		return result;

	}

}
