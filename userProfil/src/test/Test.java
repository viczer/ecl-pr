package test;

import java.util.Date;

import userProfil.Profil;
import userProfil.User;

public class Test {

	public static void main(String[] args) {
		Profil profils[] = new Profil[3];
		
		profils[0] = new Profil("CP", "Chef de Projet");
		profils[1] = new Profil("MN", "Manager");
		profils[2] = new Profil("DP", "Developpeur");
		
		User users[]= new User[3];
		users[0] = new User("Abaco", "Marco", new Date("1980/05/05"),"adressAbaco","pippo.pi@gmail.com", profils[0]);
		users[1] = new User("Alpino", "Silvio", new Date("1981/06/06"),"adressAlpino","pippo.pi@gmail.com", profils[2]);
		users[2] = new User("Amato", "Flora", new Date("1979/03/11"),"adressAmato","pippo.pi@gmail.com", profils[1]);
		
		for (User usr : users) {
			System.out.println(usr);
		}
	}

}

