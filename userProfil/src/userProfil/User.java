package userProfil;

import java.text.SimpleDateFormat;
import java.util.*;;

public class User {
	private int id;
	private String nom;
	private String prenom;
	private Date dateNais;
	private String adress;
	private String email;
	private Profil profil;
	private static int count;

	public User(String nom, String prenom, Date dateNais, String adress, String email, Profil profil) {
		super();
		this.id = ++count;
		this.nom = nom;
		this.prenom = prenom;
		this.dateNais = dateNais;
		this.adress = adress;
		this.email = email;
		this.profil = profil;

//getter setter
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public Date getDateNais() {
		return dateNais;
	}

	public void setDateNais(Date dateNais) {
		this.dateNais = dateNais;
	}

	public String getAdress() {
		return adress;
	}

	public void setAdress(String adress) {
		this.adress = adress;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
// end getter setter
//*****************************

	public User() {
		super();

	}

	@Override
	public String toString() {
		SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		String date = df.format(dateNais);
		return "Je suis le " + this.profil.getLibelle() + " " + this.nom + " " + this.prenom
				+ " mon Date de Naissance est : " + date + " mon email est : " + this.email;

	}

}
